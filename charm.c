// Copyright (C) YelloSoft
// Modified 2016-08-06 Interesting Point

#include "charm.h"
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Global Variables
int __gconio_h_ansi_attr = 0;
int __gconio_h_ansi_fg = 37;
int __gconio_h_ansi_bg = 40;

int pos_x;
int pos_y;

int get_width( void )
{
  struct winsize ws;
  ( void ) ioctl( 0, TIOCGWINSZ, &ws );

  return ( int ) ws.ws_col;

}	// end of get_width

int get_height( void )
{
  struct winsize ws;
  ( void ) ioctl( 0, TIOCGWINSZ, &ws );

  return ( int ) ws.ws_row;

}	// end of get_height

void cursor_off( void )
{
  printf( "\033[?25l" );
  ( void ) fflush( stdout );

}	// end of cursor_off

void cursor_on( void )
{
  printf( "\033[?25h" );
  ( void ) fflush( stdout );

}	// end of cursor_on

void cursor_line( void )
{
  printf( "\033[?2c" );
  ( void ) fflush( stdout );

}	// end of cursor_line

void cursor_block( void )
{
  printf( "\033[?6c" );
  ( void ) fflush( stdout );

}	// end of cursor_block

void echo_off( void )
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_lflag &= ( unsigned long ) ~ECHO;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	// end of echo_off

void echo_on(void)
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_lflag |= ECHO;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	// end of echo_on

void raw_on(void)
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_lflag &= ( unsigned long ) ~ICANON;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	// end of raw_on

void raw_off(void)
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_lflag |= ICANON;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	//end of raw_off

void blocking_off(void)
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_cc[VMIN] = (cc_t) 0;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	// end of blocking off

void blocking_on(void)
{
  struct termios term;
  ( void ) tcgetattr( fileno( stdout ), &term );
  term.c_cc[VMIN] = (cc_t) 1;
  ( void ) tcsetattr( fileno( stdout ), TCSAFLUSH, &term );

}	// end of blocking_on

int get_x(void)
{
  return pos_x;

}	// end of get_x

int get_y(void)
{
  return pos_y;

}	// end of get_y

// (0, 0) = top left
// (width - 1, 0) = top right
// (0, height - 1) = bottom left
// (width - 1, height - 1) = bottom right
void move_cursor( int x, int y )
{
  pos_x = x;
  pos_y = y;
  printf( "\033[%d;%dH", y + 1, x + 1 );
  ( void ) fflush( stdout );

}	// end of move_cursor

void blot_char(char c)
{
  if (c == '\n') {
    pos_y++;
    move_cursor( 0, pos_y );
  }
  else {
    ( void ) putchar( c );
    ( void ) fflush( stdout );
    pos_x++;
  }

}	// end of blot_char

void blot_string(const char *s)
{
  int i;

  for ( i = 0; i < ( int ) strlen( s ); i++ ) {
    blot_char( s[ i ] );
  }

}	// end of blot_string

void hcenter_string( const char *s )
{
  move_cursor( ( get_width() - ( int ) strlen( s ) ) / 2, pos_y );
  blot_string( s );

}	// end of hcenter_string

void vcenter_string( const char *s )
{
  move_cursor( ( get_width() - ( int ) strlen( s ) ) / 2 - 1, get_height() / 2 - 1 );
  blot_string( s );

}	// end of vcenter_string

void clear_screen(void)
{
  printf( "\033[2J" );
  ( void ) fflush( stdout );

}	// end of clear_screen

void __attribute__( ( noreturn ) ) handle_signal( int __attribute__( ( unused ) ) signal ) {
  end_charm();
  exit( 0 );

}	// end of handle_signal

void start_charm( void )
{
  ( void ) signal( SIGINT, handle_signal );

  cursor_off();
  echo_off();
  raw_on();
  blocking_off();
  move_cursor( 0, 0 );
  clear_screen();

}	// end of start_charm

void end_charm(void)
{
  // move_cursor( 0, 0 );
  // clear_screen();
  blocking_on();
  raw_off();
  echo_on();
  cursor_on();

}	// end of end_charm

int parse_key( char *buf )
{
  switch ( buf[ 0 ] ) {

	  case '\x7f':
	  case '\b':
	    return KEY_BACKSPACE;

	  case '\t':
	    return KEY_TAB;

	  case '\n':
	    return KEY_NEWLINE;

	  case ' ':
	  case '!':
	  case '\"':
	  case '#':
	  case '$':
	  case '%':
	  case '&':
	  case '\'':
	  case '(':
	  case ')':
	  case '*':
	  case '+':
	  case ',':
	  case '-':
	  case '.':
	  case '/':
	  case '0':
	  case '1':
	  case '2':
	  case '3':
	  case '4':
	  case '5':
	  case '6':
	  case '7':
	  case '8':
	  case '9':
	  case ':':
	  case ';':
	  case '<':
	  case '=':
	  case '>':
	  case '?':
	  case '@':
	  case 'A':
	  case 'B':
	  case 'C':
	  case 'D':
	  case 'E':
	  case 'F':
	  case 'G':
	  case 'H':
	  case 'I':
	  case 'J':
	  case 'K':
	  case 'L':
	  case 'M':
	  case 'N':
	  case 'O':
	  case 'P':
	  case 'Q':
	  case 'R':
	  case 'S':
	  case 'T':
	  case 'U':
	  case 'V':
	  case 'W':
	  case 'X':
	  case 'Y':
	  case 'Z':
	  case '[':
	  case '\\':
	  case ']':
	  case '^':
	  case '_':
	  case '`':
	  case 'a':
	  case 'b':
	  case 'c':
	  case 'd':
	  case 'e':
	  case 'f':
	  case 'g':
	  case 'h':
	  case 'i':
	  case 'j':
	  case 'k':
	  case 'l':
	  case 'm':
	  case 'n':
	  case 'o':
	  case 'p':
	  case 'q':
	  case 'r':
	  case 's':
	  case 't':
	  case 'u':
	  case 'v':
	  case 'w':
	  case 'x':
	  case 'y':
	  case 'z':
	  case '{':
	  case '|':
	  case '}':
	  case '~':
	  	return ( buf[ 0 ] );

	  case '\x1b':
	    switch ( buf[ 1 ] ) {

			case '\0':
				return KEY_ESCAPE;

			case '[':
				switch ( buf[ 2 ] ) {

					case 'A':
						return KEY_UP;

					case 'B':
						return KEY_DOWN;

					case 'C':
						return KEY_RIGHT;

					case 'D':
						return KEY_LEFT;

					case 'F':
						return KEY_END;

					case 'H':
						return KEY_HOME;

					case '1':
						switch ( buf[ 5 ] ) {

							case 'C':
									return KEY_CTRL_RIGHT;

							case 'D':
									return KEY_CTRL_LEFT;
					}

					case '2':
						return KEY_INSERT;

					case '3':
						return KEY_DELETE;

					case '5':
						return KEY_PGUP;

					case '6':
						return KEY_PGDN;
				}
			}

	  default:
	    	return KEY_UNKNOWN;
	 }

}	// end of parse_key

int get_key(void)
{
  int i;

  char *buf = (char *) malloc( 7 * sizeof( char) );

  if ( buf == NULL ) {
    return ( KEY_UNKNOWN );
  }

  buf[ 0 ] = '\0';
  buf[ 1 ] = '\0';
  buf[ 2 ] = '\0';
  buf[ 3 ] = '\0';
  buf[ 4 ] = '\0';
  buf[ 5 ] = '\0';
  buf[ 6 ] = '\0';

  i = 0;

  char c = '\0';

  ssize_t n = 0;

  // Read at least one character.
  while ( n < 1 ) {

    n = read( fileno( stdin ), &c, 1 );
  }

  while ( i < 6 && n > 0 ) {

    buf[ i++ ] = c;
    n = read( fileno( stdin ), &c, 1 );
  }

  int k = parse_key( buf );

  free( buf );

  return ( k );

}	// end of get_key

void textcolor( int newcolor )
{
	if ( newcolor > 7 ) {
		__gconio_h_ansi_attr = 1;
		newcolor -= 8;
	}
	else {
		__gconio_h_ansi_attr = 0;
	}

	switch ( newcolor ) {
		case 0:
			__gconio_h_ansi_fg = 30;
			break; // black

		case 1:
			__gconio_h_ansi_fg = 34;
			break; // blue

		case 2:
			__gconio_h_ansi_fg = 32;
			break; // green

		case 3:
			__gconio_h_ansi_fg = 36;
			break; // cyan

		case 4:
			__gconio_h_ansi_fg = 31;
			break; // red

		case 5:
			__gconio_h_ansi_fg = 35;
			break; // magenta

		case 6:
			__gconio_h_ansi_fg = 33;
			break; // brown

		case 7:
			__gconio_h_ansi_fg = 37;
			break; // gray

		default: break;
	}

	printf ("\033[%d;%d;%dm", __gconio_h_ansi_attr, __gconio_h_ansi_fg, __gconio_h_ansi_bg );

}	// end of textcolor

void textbackground( int newcolor )
{
	switch ( newcolor ) {

	case 0:
		__gconio_h_ansi_bg = 40;
		break;

	case 1:
		__gconio_h_ansi_bg = 44;
		break;

	case 2:
		__gconio_h_ansi_bg = 42;
		break;

	case 3:
		__gconio_h_ansi_bg = 46;
		break;

	case 4:
		__gconio_h_ansi_bg = 41;
		break;

	case 5:
		__gconio_h_ansi_bg = 45;
		break;

	case 6:
		__gconio_h_ansi_bg = 43;
		break;

	case 7:
		__gconio_h_ansi_bg = 47;
		break;

	default: break;
	}

	printf ("\033[%d;%d;%dm", __gconio_h_ansi_attr, __gconio_h_ansi_fg, __gconio_h_ansi_bg);

}	// end textbackground