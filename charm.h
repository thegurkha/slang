#ifndef CHARM_HEADER
#define CHARM_HEADER

// Copyright (C) YelloSoft
// Modified 2016-06-08 Interesting Point

#define CHARM_VERSION "0.0.1"

extern int pos_x;
extern int pos_y;

// Global Variables
extern int __gconio_h_ansi_attr;
extern int __gconio_h_ansi_fg;
extern int __gconio_h_ansi_bg;

int get_width( void );
int get_height( void );

void cursor_off( void );
void cursor_on( void );
void cursor_line( void );
void cursor_block( void );

void echo_off( void );
void echo_on( void );

void raw_on( void );
void raw_off(void );

void blocking_off( void );
void blocking_on( void );

int get_x( void );
int get_y( void );
void move_cursor( int x, int y );
void blot_char( char c );
void blot_string( const char *s );
void hcenter_string( const char *s );
void vcenter_string( const char *s );

void clear_screen( void );

void handle_signal( int signal );
void start_charm( void );
void end_charm( void );

#define BLACK 					0
#define BLUE 						1
#define GREEN 					2
#define CYAN 						3
#define RED 						4
#define MAGENTA 				5
#define BROWN 					6
#define LIGHTGRAY 			7
#define DARKGRAY 				8
#define LIGHTBLUE 			9
#define LIGHTGREEN 			10
#define LIGHTCYAN 			11
#define LIGHTRED 				12
#define LIGHTMAGENTA		13
#define YELLOW 					14
#define WHITE 					15

#define	KEY_BACKSPACE		0X0008
#define	KEY_TAB					0x0009
#define	KEY_NEWLINE			0X000D
#define	KEY_ESCAPE			0x001B
#define	KEY_UP					0x8048
#define	KEY_DOWN				0x8050
#define	KEY_RIGHT				0x804D
#define	KEY_LEFT				0x804B
#define	KEY_END					0x804F
#define	KEY_HOME				0x8047
#define	KEY_CTRL_RIGHT	0x8074
#define	KEY_CTRL_LEFT		0x8073
#define	KEY_INSERT			0x8052
#define	KEY_DELETE			0x8053
#define	KEY_PGUP				0x8049
#define	KEY_PGDN				0x8051
#define	KEY_UNKNOWN			0x0000

int parse_key( char *buf );
int get_key( void );

#endif
