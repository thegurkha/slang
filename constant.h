/*
 * constant definitions and macros for the
 * slang language interpreter
 *
 * (c) Dave McKay 1996
 *
 */

#define Lastch(s)			s[strlen(s)-1]
#define TrimCR(s)			if (Lastch(s) == '\n') Lastch(s) = '\x00'

/* start of Gregorian calendar */
#define START_GREGORIAN ( 15 + 31L * ( 10 * 12L * 1582 ) )

// timer routines
#define NANO_SECOND_MULTIPLIER  1000000     // 1 millisecond = 1,000,000 Nanoseconds
const long INTERVAL_MS = 85 * NANO_SECOND_MULTIPLIER;

/* mathematical constants */
#define PI							3.1415926535
#define RADIAN					57.272728

#define TRUE						0
#define FALSE						1

#define MAX_LINES				1000
#define MAX_IF_DEPTH		64
#define	MAX_FOR_DEPTH		64
#define	MAX_WHILE_DEPTH	64
#define	MAX_DO_DEPTH		64
#define	MAX_PROC_DEPTH	1000

#define	GLOBAL				  0
#define	ALL					    (-1)
#define	CONSTANT				(-1)

#define	CALCULATE				0
#define	PRINT						1

#define NOT_IN_IF				0
#define IF_WORKING 			1
#define IF_DONE					2
#define IF_SEEK					3

#define	FIX							0
#define	SET							1

#define	GENERAL 				0
#define	SYNTAX					1

#define	pINTEGER 				1
#define	pSTRING					2
#define	pFLOAT					3
#define	pDATA 					4

#define OFF           	0
#define FULL          	1
#define ERROR         	2
#define TERMINATE     	3

#define DELIMITER     	1
#define NUMBER        	2

#define BELL          '\x07'
#define DOLLAR        '$'
#define LEFT_PAREN    '('
#define RIGHT_PAREN		')'
#define EQUALS				'='
#define GREATER				'>'
#define LESSER				'<'
#define POSITIVE 			'+'
#define NEGATIVE			'-'
#define NOT						'!'
#define AND						'&'
#define OR						'|'
#define POINT					'.'
#define HASH					'#'
#define SPACE					0x0020
#define RETURN				'\n'
#define COLON					':'
#define PERCENT				'%'
#define QUOTE 				'"'
#define SQUOTE 				0x0027
#define COMMENT				'/'
#define FSLASH				0x5C
#define EOS						'\x00'

#define FLOAT 				'f'
#define INTEGER				'd'
#define STRING 				's'

#define NO_FILE							1
#define BAD_FILE						2
#define NO_MEMORY						3
#define FILE_TOO_BIG				4
#define	UNKNOWN_PASSEE			5
#define GENERAL_ERROR				6
#define STRAY_CHARACTER			7
#define UNKNOWN_VARIABLE		8
#define BAD_PRIMITIVE				9
#define UNBALANCED_PAR			10
#define WRONG_LOG_OP				11
#define INVALID_LOG_OP			12
#define INVALID_RHS					13
#define NO_IF_CONDITIONAL		14
#define NO_IF_BRACKETS			15
#define DIV_BY_ZERO					16
#define SPACES_IN_NAME			17
#define DUPLICATE_LABEL			18
#define MISPLACED_STR				19
#define UNKNOWN_LABEL				20
#define BAD_ENDIF						21
#define UNBALANCED_IF				22
#define IF_TOO_DEEP					23
#define GOTO_OUT_IF					24
#define DUPLICATE_PROC			25
#define NO_PROC							26
#define UNKNOWN_PROC				27
#define NO_PROC_NAME				28
#define NO_MAIN							29
#define BAD_ENDPROC					30
#define PROC_TOO_DEEP				31
#define BAD_TOKEN 					32
#define	BAD_UNARY						33
#define BAD_ELSE_LOG				34
#define	NOT_A_STR_VAR				35
#define	MISPLACED_EXP				36
#define NUM_LOG							37
#define GLOBAL_NO_VAR				38
#define LOCAL_NO_VAR				39
#define VAR_EXISTS					40
#define VAR_NOT_EXIST				41
#define BAD_PASS_GLOBAL			42
#define NOT_TAKE_PAR				43
#define MISMATCHED_PARS			44
#define NO_PARS_PASSED			45
#define UNRECOG_OPTION			46
#define BAD_PASS_NUMERIC		47
#define TOO_FEW_PARS				48
#define TOO_MANY_PARS				49
#define NO_FOR_CONDITIONAL	50
#define NO_FOR_TO_CLAUSE		51
#define NO_STEP_VALUE				52
#define FOR_TOO_DEEP				53
#define BAD_FOR_VARIABLE		54
#define CANNOT_MOD_CON			55
#define ENDFOR_WITH_PARS		56
#define MISPLACED_ENDFOR		57
#define UNBALANCED_FOR			58
#define CONSTANT_NO_VAR			59
#define BAD_PASS_CONSTANT		60
#define DO_TOO_DEEP 				61
#define BAD_DO_LOG 					62
#define NO_WHILE_CONDITION	63
#define NO_WHILE_BRACKETS		64
#define WHILE_TOO_DEEP			65
#define ENDWHILE_WITH_PARS	66
#define MISPLACED_ENDWHILE	67
#define UNBALANCED_WHILE		68
#define FUNC_NO_PARS				69
#define GENERIC_MISS_PAR		70
#define NO_UNTIL_CONDITION	71
#define BAD_LITERAL					72
#define NO_UNTIL_BRACKETS		73
#define BAD_MIDSTR_PAR			74
#define CALLING_MAIN				75
#define DUPLICATE_FILE			76
#define UNKNOWN_FILE 				77
#define INVALID_FILENAME		78
#define INVALID_FMODE				79
#define OPEN_FAILED					80
#define FILE_NOT_OPEN				81
#define FILE_ALREADY_OPEN		82
#define BAD_FLUSHALL				83
#define PRN_BUSY_OFF				84
#define PRN_DISCONNECT			85
#define FILE_STILL_OPEN			86
#define CANNOT_DELETE				87
