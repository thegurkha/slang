/*
 * SLANG Interpreter (c) D. McKay 1996-2016
 *
 * Header file for the slang interpreter
 *
 */


#include	<stdio.h>
#include	<termios.h>
#include	<unistd.h>
#include	<stdarg.h>
#include	<malloc.h>
#include	<string.h>
#include	<stdlib.h>
#include	<ctype.h>
#include	<math.h>
#include	<time.h>

#include	"constant.h"

#ifndef EXTERNAL_HEADER_GUARD
#define EXTERNAL_HEADER_GUARD

#define CLS system("clear")

// global flags and arrays
int LINES_READ, CURRENT_LINE, STRING_VAR, DEBUG, FOR_DEPTH, WIDTH;
int IF_NESTING, DEEPEST_IF, PROC_RETURNS, PROC_CALLS, DEEPEST_FOR;
int DEEPEST_PROC, WHILE_DEPTH, DEEPEST_WHILE, MAX_PARS, DO_DEPTH, DEEPEST_DO;

int SEEK_NEST[ MAX_IF_DEPTH ];
int IF_NEST[ MAX_IF_DEPTH ];
int RETURN_NEST[ MAX_PROC_DEPTH ];
int PROC_DEPTH[ MAX_PROC_DEPTH ];

// forward declaration
typedef struct vList vList;

// used to hold the starting elements of each linked list
vList *vTable[ MAX_PROC_DEPTH ];

// forward declaration
typedef struct Label Label;

// Start of Label Linked List and End of Label Linked List markers
Label *LabelStart, *LastLabel;

// forward declaration
typedef struct Proc Proc;

// Start of Procedure Linked List and End of Procedure Linked List markers
Proc *ProcStart, *LastProc;

// forward declaration
typedef struct File File;

// Start of File Linked List and End of File Linked List markers
File *FileStart, *LastFile;

// forward declaration
typedef struct For For;

// used to hold the for structures
For *fTable[ MAX_FOR_DEPTH ];

// forward declaration
typedef struct While While;

// used to hold the while structures
While *wTable[ MAX_WHILE_DEPTH ];

// forward declaration
typedef struct Do Do;

// used to hold the do structures
Do *dTable[ MAX_DO_DEPTH ];

// string, dynamic string, integer and float scratch pad workspace
char Scratch1[ 200 ], Scratch2[ 200 ], Scratch3[ 200 ];
char *dScratch1, *dScratch2, *dScratch3;
long iScratch1, iScratch2, iScratch3, iScratch4;
double iFloat1, iFloat2, iFloat3;

// used to hold the System Time values
int sHour, sMin, sSec, sHund;

// used to hold the System Date values
int dYear, dMonth, dDay;

// handy pointers
char *Function, *Syntax;

// used to hold the individual parameters
char Par1[ 80 ], Par2[ 80 ], Par3[ 80 ], Par4[ 80 ];
char Par5[ 80 ], Par6[ 80 ], Par7[ 80 ], Par8[ 80 ];

// parameters collectively held as an array of pointers
char *Variables[]={
		Par1,
		Par2,
		Par3,
		Par4,
		Par5,
		Par6,
		Par7,
		Par8,
		NULL
};

// used to hold the read-in source code lines
char *Source[MAX_LINES];

// used to hold the Month names
char *MonthNames[]={
		"",
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
		NULL
};

// used to hold the Day names
char *DayNames[]={
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		NULL
};

// Reserved Words
char *Command[]={
		"CLS",				// 00
		"BEEP",				// 01
		"PRINT",			// 02
		"PAUSE",			// 03
		"IF",					// 04
		"ELIF",				// 05
		"ELSE",				// 06
		"ENDIF",			// 07
		"GOTO",				// 08
		"DEBUG",			// 09
		"DEFPROC",		// 10
		"PROC",  			// 11
		"ENDPROC",		// 12
		"RETURN",			// 13
		"SYSTEM",			// 14
		"GLOBAL",			// 15
		"LOCAL",			// 16
		"FOR", 				// 17
		"ENDFOR",			// 18
		"CONSTANT",		// 19
		"DECIMAL",		// 20
		"AT", 				// 21
		"WHILE", 			// 22
		"ENDWHILE",		// 23
		"DO",					// 24
		"UNTIL",			// 25
		"TONE",				// 26
		"STOP",				// 27
		"STRLEN", 		// 28
		"DAYNAME", 		// 29
		"CURSOR", 		// 30
		"MONTHNAME",	// 31
		"HOUR",				// 32
		"MINUTE",			// 33
		"SECOND",			// 34
		"HUNDRED",		// 35
		"YEAR"	,			// 36
		"MONTH"	,			// 37
		"DAY"	,				// 38
		"SUFFIX",			// 39
		"FOREGROUND",	// 40
		"BACKGROUND",	// 41
		"LEFTSTR",		// 42
		"RIGHTSTR",		// 43
		"LOWERSTR",		// 44
		"UPPERSTR",		// 45
		"COPYSTR",		// 46
		"STRCAT",			// 47
		"INTSTR",			// 48
		"STRINT",			// 49
		"NUMSTR",			// 50
		"STRNUM",			// 51
		"STRLOC",			// 52
		"STRCMP",			// 53
		"STRCMPI",		// 54
		"ASCVAL",			// 55
		"STRREV",			// 56
		"MIDSTR",			// 57
		"GETVAL",			// 58
		"GETCHAR",		// 59
		"PUTVAL",			// 60
		"PUTCHAR",		// 61
		"REPSTR",			// 62
		"ABS",				// 63
		"FABS",				// 64
		"RAND",				// 65
		"FRAND",			// 66
		"TICKER",			// 67
		"INPUT",			// 68
		"GETXY",			// 69
		"EDIT",				// 70
		"GETJD",			// 71
		"JDATE",			// 72
		"SIN",				// 73
		"COS",				// 74
		"TAN",				// 75
		"ASIN",				// 76
		"ACOS",				// 77
		"ATAN",				// 78
		"RAD",				// 79
		"DEG",				// 80
		"EXP",				// 81
		"SQR",				// 82
		"LN",					// 83
		"LOG",				// 84
		"FILE",				// 85
		"CLOSE",			// 86
		"OPEN",				// 87
		"FPRINT",			// 88
		"FGETS",			// 89
		"KILL",				// 90
		"VPRINT",			// 91
		"PUTS",				// 92
		"FPUTS",			// 93
		"FGETPOS",		// 94
		"FSETPOS",		// 95
		"FLUSH",			// 96
		"FLUSHALL",		// 97
		"CLOSEALL",		// 98
		"LPRINT",			// 99
		"UNLINK",			// 100
		NULL          // end
};

enum ReservedWords {
	rCLS,					// 0
	rBEEP,
	rPRINT,
	rPAUSE,
	rIF,
	rELIF,
	rELSE,
	rENDIF,
	rGOTO,
	rDEBUG,
	rDEFPROC,			// 10
	rPROC,
	rENDPROC,
	rRETURN,
	rSYSTEM,
	rGLOBAL,
	rLOCAL,
	rFOR,
	rENDFOR,
	rCONSTANT,
	rDECIMAL,			// 20
	rAT,
	rWHILE,
	rENDWHILE,
	rDO,
	rUNTIL,
	rTONE,
	rSTOP,
	rSTRLEN,
	rDAYNAME,
	rCURSOR,				// 30
	rMONTHNAME,
	rHOUR,
	rMINUTE,
	rSECOND,
	rHUNDRED,
	rYEAR,
	rMONTH,
	rDAY,
	rSUFFIX,
	rFOREGROUND,		// 40
	rBACKGROUND,
	rLEFTSTR,
	rRIGHTSTR,
	rLOWERSTR,
	rUPPERSTR,
	rCOPYSTR,
	rSTRCAT,
	rINTSTR,
	rSTRINT,
	rNUMSTR,				// 50
	rSTRNUM,
	rSTRLOC,
	rSTRCMP,
	rSTRCMPI,
	rASCVAL,
	rSTRREV,
	rMIDSTR,
	rGETVAL,
	rGETCHAR,
	rPUTVAL,				// 60
	rPUTCHAR,
	rREPSTR,
	rABS,
	rFABS,
	rRAND,
	rFRAND,
	rTICKER,
	rINPUT,
	rGETXY,
	rEDIT,				// 70
	rGETJD,
	rJDATE,
	rSIN,
	rCOS,
	rTAN,
	rASIN,
	rACOS,
	rATAN,
	rRAD,
	rDEG,					// 80
	rEXP,
	rSQR,
	rLN,
	rLOG,
	rFILE,
	rCLOSE,
	rOPEN,
	rFPRINT,
	rFGETS,
	rKILL,				// 90
	rVPRINT,
	rPUTS,
	rFPUTS,
	rFGETPOS,
	rFSETPOS,
	rFLUSH,
	rFLUSHALL,
	rCLOSEALL,
	rLPRINT,
	rUNLINK,		// 100

};

char *Express, *ExpressPtr, *Program, *Version, *DeepestProc;
char Token[80], TokenType;
FILE *ScriptFile;

char *ErrorMessage[]={
	"Dummy Entry",
	"A Source file must be specified on the command line",									// NO_FILE												1
	"There is a problem with the Source file you specified",								// BAD_FILE												2
	"Memory allocation failure: couldn't allocate core memory",							// NO_MEMORY											3
	"There are too many lines in the Source file",													// FILE_TOO_BIG										4
	"Passing an undeclared variable in a PROC call",												// UNKNOWN_PASSEE									5
	"General and/or logical error occurred",																// GENERAL_ERROR									6
	"Stray characters detected (badly formatted Line)",											// STRAY_CHARACTER								7
	"Undeclared variable name encountered",																	// UNKNOWN_VARIABLE								8
	"Syntax error encountered",																							// BAD_PRIMITIVE									9
	"Unbalanced parentheses in an expression",															// UNBALANCED_PAR									10
	"Incorrect logical operator in an expression",													// WRONG_LOG_OP										11
	"Invalid logical operator in an expression",														// INVALID_LOG_OP									12
	"Invalid right hand side of expression",																// INVALID_RHS										13
	"No conditional expression connected with an IF or ELIF statement",			// NO_IF_CONDITIONAL							14
	"IF and ELIF conditional expressions must be bounded by brackets",			// NO_IF_BRACKETS									15
	"An attempt was made to divide by zero",																// DIV_BY_ZERO										16
	"Variable names must not included embedded spaces",											// SPACES_IN_NAME									17
	"Duplicate line Label encountered",																			// DUPLICATE_LABEL								18
	"Wrong parameter passed instead of a numeric parameter",								// MISPLACED_STR									19
	"GOTO with an unknown label",																						// UNKNOWN_LABEL									20
	"ENDIF encountered with no matching IF",																// BAD_ENDIF											21
	"Unbalanced IF/ENDIF(s) active when the program ended",									// UNBALANCED_IF									22
	"Maximum depth of IF/ENDIF nesting has been reached",										// IF_TOO_DEEP										23
	"GOTO out of a nested IF statement",																		// GOTO_OUT_IF										24
	"Duplicate procedure name encountered",																	// DUPLICATE_PROC									25
	"PROC command without a procedure name",																// NO_PROC												26
	"PROC with an unknown procedure name",																	// UNKNOWN_PROC										27
	"DEFPROC without a procedure name",																			// NO_PROC_NAME										28
	"No Main procedure in the Source file",																	// NO_MAIN												29
	"Internal error - PROC_CALLS < 0",																			// BAD_ENDPROC										30
	"Procedure nesting has reached the maximum permissible depth!",					// PROC_TOO_DEEP									31
	"Badly formed token in numerical expression",														// BAD_TOKEN 											32
	"Misplaced Unary operator in logical expression",												// BAD_UNARY											33
	"A logical expression is not required for ELSE statements",							// BAD_ELSE_LOG										34
	"Numeric parameter passed instead of a string parameter",								// NOT_A_STR_VAR									35
	"Expression passed instead of a numeric variable",											// MISPLACED_EXP									36
	"Misplaced numerical operators in logical expression",									// NUM_LOG												37
	"GLOBAL statement with no associated variable(s)",											// GLOBAL_NO_VAR									38
	"LOCAL statement with no associated variable(s)",												// LOCAL_NO_VAR										39
	"Redeclaration of an existing variable",																// VAR_EXISTS											40
	"Referencing an unknown variable",																			// VAR_NOT_EXIST									41
	"GLOBAL variables cannot be passed by reference",												// BAD_PASS_GLOBAL								42
	"Passing parameters to a procedure that does not accept any",						// NOT_TAKE_PAR										43
	"Type of passed parameters do not match procedure definition",					// MISMATCHED_PARS								44
	"Calling a procedure without the required parameters",									// NO_PARS_PASSED									45
	"Unrecognised option for this statement",																// UNRECOG_OPTION									46
	"Literal numerical values cannot be passed by reference",								// BAD_PASS_NUMERIC								47
	"Too few parameters in PROC call",																			// TOO_FEW_PARS										48
	"Too many parameters in PROC call",																			// TOO_MANY_PARS									49
	"No conditional statement connected with a FOR statement",							// NO_FOR_CONDITIONAL							50
	"No TO clause in a FOR statement",																			// NO_FOR_TO_CLAUSE								51
	"STEP clause with no associated step value in a FOR statement",					// NO_STEP_VALUE									52
	"Maximum depth of FOR nesting has been reached",												// FOR_TOO_DEEP										53
	"String variables cannot be used in FOR loops",													// BAD_FOR_VARIABLE								54
	"Attempt to modify the value of a constant variable",										// CANNOT_MOD_CON									55
	"The ENDFOR statement accepts no parameters",														// ENDFOR_WITH_PARS								56
	"ENDFOR encountered with no matching FOR",															// MISPLACED_ENDFOR								57
	"Unbalanced FOR/ENDFOR(s) active when the program ended",								// UNBALANCED_FOR									58
	"CONSTANT statement with no associated variable(s)",										// CONSTANT_NO_VAR								59
	"CONSTANT variables cannot be passed by reference",											// BAD_PASS_CONSTANT							60
	"Maximum depth of DO/UNTIL nesting has been reached",										// DO_TOO_DEEP 										61
	"The DO statement accepts no parameters",																// BAD_DO_LOG 										62
	"No conditional expression connected with a WHILE statement",						// NO_WHILE_CONDITION							63
	"WHILE conditional expressions must be bounded by brackets",						// NO_WHILE_BRACKETS							64
	"Maximum depth of WHILE/ENDWHILE nesting has been reached",							// WHILE_TOO_DEEP									65
	"The ENDWHILE statement accepts no parameters",													// ENDWHILE_WITH_PARS							66
	"ENDWHILE encountered with no matching WHILE",													// MISPLACED_ENDWHILE							67
	"Unbalanced WHILE/ENDWHILE(s) active when the program ended",						// UNBALANCED_WHILE								68
	"Statement without the required parameters",														// FUNC_NO_PARS										69
	"Missing Parameter in SLANG function call",															// GENERIC_MISS_PAR								70
	"No conditional expression connected with a DO statement",							// NO_UNTIL_CONDITION 						71
	"Literal value passed instead of a variable name",											// BAD_LITERAL										72
	"DO conditional expressions must be bounded by brackets",								// NO_UNTIL_BRACKETS							73
	"Invalid values in the MIDSTR statement parameters",										// BAD_MIDSTR_PAR									74
	"Invalid call to Main procedure",																				// CALLING_MAIN										75
	"Duplicate filename encountered",																				// DUPLICATE_FILE									76
	"Reference to an unknown file identifier",															// UNKNOWN_FILE 									77
	"Invalid format for a filename parameter",															// INVALID_FILENAME								78
	"Invalid open file mode symbol (valid symbols: w, r, t, b, +)",					// INVALID_FMODE									79
	"File open request failed",																							// OPEN_FAILED										80
	"File has not been opened",																							// FILE_NOT_OPEN									81
	"File has already been opened",																					// FILE_ALREADY_OPEN							82
	"The FLUSHALL statement accepts no parameters",													// BAD_FLUSHALL										83
	"The printer is either busy or switched off",														// PRN_BUSY_OFF										84
	"The printer seems to be disconnected",																	// PRN_DISCONNECT									85
	"Cannot unlink (delete) an open file",																	// FILE_STILL_OPEN								86
	"Cannot unlink (delete) the specified file" 														// CANNOT_DELETE									87
};

#else

extern int LINES_READ, CURRENT_LINE, STRING_VAR, DEBUG, FOR_DEPTH, WIDTH;
extern int IF_NESTING, DEEPEST_IF, PROC_RETURNS, PROC_CALLS, DEEPEST_FOR;
extern int DEEPEST_PROC, SEEK_NEST[MAX_IF_DEPTH], IF_NEST[MAX_IF_DEPTH];
extern int RETURN_NEST[MAX_PROC_DEPTH], PROC_DEPTH[MAX_PROC_DEPTH];
extern int WHILE_DEPTH, DEEPEST_WHILE, MAX_PARS;
extern int DO_DEPTH, DEEPEST_DO;

// string, dynamic string, integer and float scratch pad workspace
extern char Scratch1[], Scratch2[], Scratch3[];
extern char *dScratch1, *dScratch2, *dScratch3;
extern long iScratch1, iScratch2, iScratch3, iScratch4;
extern double iFloat1, iFloat2, iFloat3;

// used to hold the System Time values
extern int sHour, sMin, sSec, sHund;

// used to hold the System Date values
extern int dYear, dMonth, dDay;

// handy pointers
extern char *Function, *Syntax;

// used to hold the individual parameters
extern char Par1[], Par2[], Par3[], Par4[];
extern char Par5[], Par6[], Par7[], Par8[];

extern struct vList *vTable[];
extern struct Label *LabelStart, *LastLabel;
extern struct Proc *ProcStart, *LastProc;
extern struct File *FileStart, *LastFile;
extern struct For *fTable[];
extern struct While *wTable[];
extern struct Do *dTable[];

// parameters collectively held as an array of pointers
extern char *Variables[];

// used to hold the read-in source code lines
extern char *Source[];

// used to hold the Month names
extern char *MonthNames[];

// used to hold the Day names
extern char *DayNames[];

// Reserved Words
char *Command[];

extern char *Express, *ExpressPtr, *Program, *Version, *DeepestProc;
extern char Token[], TokenType;
extern FILE *ScriptFile;

extern char *ErrorMessage[];

#endif
