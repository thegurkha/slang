#include	"sl.h"
#include	"header.h"
#include	"structs.h"
#include	"charm.h"
#include	"math.h"
#include	<alsa/asoundlib.h>

int main( int argc, char *argv[] )
{
	Program = "SLANG Interpreter (c) D. McKay 1996-2025";
	Version = "Version gcc v.0.2.2";

	if ( argc == 1 ) {
		ReportError( GENERAL, NO_FILE, NULL );
	}

	if ( strcmp( "-r", argv[ 1 ] ) == 0 ) {
		CLS;
		printf( "%s\n%s %s %s\n", Program, Version, __DATE__, __TIME__ );
		ShowReservedWords();
	}
	else if ( ( strcmp( "-h", argv[ 1 ] ) == 0 ) || ( strcmp( "-help", argv[ 1 ] ) == 0 ) ) {
		// call help function
		HelpRequest( ( argv[ 2 ] != NULL ) ? argv[ 2 ] : NULL );
	}
	else {
		start_charm();
		ReadInSourceFile( argv[ 1 ] );
		InterpretSourceFile();
		end_charm();
	}

	exit( 0 );

}	// end of main

void Initialise( char *FileName )
{
	int i;

	char szEOFValue[]="-1";
	char szNULLValue[]="0";

	// zero the "remember the nesting" arrays
	for ( i=0; i<MAX_IF_DEPTH; i++ ) {
		IF_NEST[ i ]=NOT_IN_IF;
		SEEK_NEST[ i ]=0;
		RETURN_NEST[ i ]=0;
	}

	// zero the for table array
	for ( i=0; i<MAX_FOR_DEPTH; i++ )
			fTable[ i ]=NULL;

	// zero the while table array
	for ( i=0; i<MAX_WHILE_DEPTH; i++ )
			wTable[ i ]=0;

	// zero the do table array
	for ( i=0; i<MAX_DO_DEPTH; i++ )
			dTable[ i ]=0;

	// start off NOT in an IF loop
	IF_NESTING=0;

	// default value for the number of decimal places printed
	WIDTH=2;

	// assume DEBUG off to start with
	DEBUG=OFF;

	// no lines read in yet
	PROC_CALLS = CURRENT_LINE = LINES_READ = 0;

	// no proc calls nesting yet
	DEEPEST_PROC = 0;
	DeepestProc = NULL;

	// no if or for nesting encountered yet
	DEEPEST_IF=DEEPEST_DO=DEEPEST_FOR=0;

	// no For loops encountered yet
	FOR_DEPTH = 0;

	// set the maximum number of parameters we can send to a procedure
	MAX_PARS = 8;

	// no labels in the table yet
	LabelStart=LastLabel=NULL;

	// scratch-pad for mathematical resolutions
	Express = ( char * ) malloc( 250 );

	if ( Express == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for char *Express" );

	// system global variables accessible to SLANG programs
	FixVariable( "_PROGRAM$", Program, CONSTANT, NULL );
	FixVariable( "_VERSION$", Version, CONSTANT, NULL );
	FixVariable( "_SOURCEFILE$", FileName, CONSTANT, NULL );
	FixVariable( "_EOF%", szEOFValue, CONSTANT, NULL );
	FixVariable( "_NULL%", szNULLValue, CONSTANT, NULL );

	sprintf( Scratch1, "%02d.%02d", 16, 04 );
	FixVariable( "_OS_VERSION", Scratch1, CONSTANT, NULL );

	srand( ( unsigned ) time( NULL ) % 37 );

}	// end of Initialise

void ReadInSourceFile( char *FileName )
{
	static char InText[ 100 ]="";
	int Response, Line=0;

	Initialise( FileName );

	ScriptFile=fopen( FileName, "rt" );

	if ( ScriptFile == NULL )
			ReportError( GENERAL, BAD_FILE, "Can't open the Source file: %s",
					FileName );

    // line by line read in the script file
    while ( fgets( InText, 99, ScriptFile ) != NULL ) {

		if ( InText[ 0 ] == RETURN )
				continue;

        // count the lines as they go by
				CURRENT_LINE = ++Line;

        if ( ++LINES_READ >= MAX_LINES )
				ReportError( GENERAL, FILE_TOO_BIG, "Program %s too long ( over %04d Lines )", FileName, MAX_LINES );

		if ( InText[ 1 ] != EOS )
				TrimCR( InText );

		// trim off all redundant whitespace
		RemoveWhitespace( InText );

		if ( ( Source[ Line ] = ( char * ) strdup( InText ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Source[ %03d ]", Line );

		if ( Source[ Line ][ 0 ] == COLON )
				AddLabel( Source[ Line ], Line );

		 else {
		 	Response = GetCommandWord( PullFirstWord( InText ), "DEFPROC" );

			if ( Response == 0 )
					AddProc( InText, Line );
		}

		BlankString( InText, EOS, 199 );
	}

	CURRENT_LINE = 0;

}   // end of ReadInSourceFile

int SetToUppercase( char *Text )
{
	int i;

	// force string letter by letter to uppercase
	for ( i = 0; Text[ i ] != 0x00; i++ ) {

		Text[ i ] = toupper( Text[ i ] );
	}

	return ( 0 );

}	// end of SetToUppercase

int SetToLowercase( char *Text )
{
	int i;

	// force string letter by letter to lowercase
	for ( i=0; Text[ i ] != 0x00; i++ ) {

		Text[ i ]=tolower( Text[ i ] );
	}

	return ( 0 );

}	// end of SetToLowercase

void MemoryDump( void )
{
	int i;
	vList *vPtr;
	Label *Lptr;
	File *fPtr;
	Proc *ProcPtr;

	printf( "%s\n%s %s %s\n", Program, Version, __DATE__, __TIME__ );

	printf( "\n==[ Variables active at closedown ]=============================================\n" );

	for ( i=0; i<MAX_PROC_DEPTH; i++ ) {

		if ( vTable[ i ] != NULL ) {

			for ( vPtr=vTable[ i ]; vPtr != NULL; vPtr = vPtr->Next ) {

				printf( "%-18s %c <%03d> %c%c %48s\n",
						vPtr->vName,
						vPtr->Type,
						i,
						( vPtr->Passed != NULL ) ? 'P' : ' ',
						( vPtr->Constant == TRUE ) ? 'C' : ' ',
						vPtr->vValue );
			}
		}
	}

	printf( "\n==[ Labels ]====================================================================\n" );

	if ( LabelStart == NULL )
			puts( "No Labels established" );
	else {
		for ( Lptr = LabelStart; Lptr != NULL; Lptr = Lptr->Next )
				printf( "%-20s %03d\n", Lptr->Name, Lptr->Line );
	}

	printf( "\n==[ File definitions active at closedown ]======================================\n" );

	if ( FileStart == NULL )
			puts( "No definitions active" );
	else {
		for ( fPtr = FileStart; fPtr != NULL; fPtr = fPtr->Next )
				printf( "%-50s %s\n", fPtr->Name,
				( fPtr->Open == TRUE ) ? "Open" : "Closed" );
	}

	printf( "\n==[ Procedures ]================================================================\n" );

	for ( ProcPtr = ProcStart; ProcPtr != NULL; ProcPtr = ProcPtr->Next ) {
		RemoveCommandWord( Source[ ProcPtr->Line ], "DEFPROC" );
		printf( " %-70s %03d\n",
				Source[ ProcPtr->Line ], ProcPtr->Line );
	}

	printf( "\n==[ Program Statistics ]========================================================\n" );

	if ( PROC_CALLS >= 1 ) {
		vPtr=LookUp( "_ProcName$", PROC_CALLS );
		printf( "%40s: %s\n", "Procedure Active at closedown", vPtr->vValue );
	}
	else
			printf( "%40s: %s\n", "Procedure Active at closedown", "Main" );

	printf( "%40s: %03d\n", "Procedure Nesting Level at closedown", PROC_CALLS );
	printf( "%40s: %s\n", "Deepest Nested Procedure", DeepestProc );
	printf( "%40s: %03d\n", "Deepest Procedure Nesting Level reached", DEEPEST_PROC );
	printf( "%40s: %03d\n", "IF Nesting Level at closedown", IF_NESTING );
	printf( "%40s: %03d\n", "Deepest IF Nesting Level reached", DEEPEST_IF );
	printf( "%40s: %03d\n", "FOR Nesting Level at closedown", FOR_DEPTH );
	printf( "%40s: %03d\n", "Deepest FOR Nesting Level reached", DEEPEST_FOR );
	printf( "%40s: %03d\n", "WHILE Nesting Level at closedown", WHILE_DEPTH );
	printf( "%40s: %03d\n", "Deepest WHILE Nesting Level reached", DEEPEST_WHILE );
	printf( "%40s: %03d\n", "DO Nesting Level at closedown", DO_DEPTH );
	printf( "%40s: %03d\n", "Deepest DO Nesting Level reached", DEEPEST_DO );

}	// end of MemoryDump

void ShowReservedWords( void )
{
	int i;

	printf( "\n==[ Reserved Words ]============================================================\n" );

	for ( i=0; Command[ i ] != NULL; i++ ) {
		if ( Command[ i ] == NULL )
				break;
		else
				printf( "%-12s%c", Command[ i ],
						( ( ( i+1 ) % 6 ) == 0 ) ? RETURN : SPACE );
	}

	printf( "\n" );

}	// end of ShowReservedWords

int kbhit( void )
{
	struct timeval tv;
	fd_set read_fd;

	tv.tv_sec=0;
	tv.tv_usec=0;

	FD_ZERO( &read_fd );
	FD_SET( 0,&read_fd );

	if ( select( 1, &read_fd, NULL, NULL, &tv ) == ( -1 ) )
		return ( 0 );

	if ( FD_ISSET( 0,&read_fd ) )
			return ( 1 );

	return ( 0 );

}	// end of kbhit

void AtStatement( char *Text )
{
	ExtractParameters( "AT", "II", Text );

	iScratch1=atol( Par1 );
	iScratch2=atol( Par2 );

	GotoScreenXY( ( int ) iScratch1, ( int ) iScratch2 );

}	// end of AtStatement

void GotoScreenXY( int Xpos, int Ypos )
{
	Xpos = ( Xpos > 25 ) ? 25 : Xpos;
	Xpos = ( Xpos < 1 ) ? 1 : Xpos;

	Ypos = ( Ypos > 80 ) ? 80 : Ypos;
	Ypos = ( Ypos < 1 ) ? 1 : Ypos;

	move_cursor( Xpos, Ypos );

}	// end of GotoScreenXY

void DecimalStatement( char *Text )
{
	ExtractParameters( "DECIMAL", "I", Text );

	WIDTH=atoi( Par1 );

	WIDTH = ( WIDTH < 0 ) ? 0 : WIDTH;

}	// end of DecimalStatement

void ToneStatement( char *Text )
{
	ExtractParameters( "TONE", "II", Text );

	PlaySound( atoi( Par1 ), atoi( Par2 ) );

}	// end of ToneStatement

void PlaySound( int freq, int duration )
{
  snd_pcm_t *handle;

  int i, err;
  int fs = 3000;    // sampling frequency

  // sanity check frequency values
  freq = ( freq > 2000 ) ? 2000 : freq;
  freq = ( freq < 50 ) ? 50 : freq;

  // sanity check duration values
  duration = ( duration > 1000 ) ? 1000 : duration;
  duration = ( duration < 100 ) ? 100 : duration;

  duration *= 3.3;

  float buffer[ duration ];

  // fill buffer with sin wave
  for ( i=0; i<duration; i++) {
    buffer[ i ] = ( sin ( 2*M_PI*freq/fs*i ) );
  }

  if (snd_pcm_open (&handle, "default", SND_PCM_STREAM_PLAYBACK, 0) < 0) {
    printf ("snd_pcm_open -- failed to open device\n");
    exit ( 1 );
  }

  if ( ( err = snd_pcm_set_params(handle, SND_PCM_FORMAT_FLOAT, SND_PCM_ACCESS_RW_INTERLEAVED, 1, fs, 1, 50 ) ) < 0 ) {
    printf( "Playback open error: %s\n", snd_strerror( err ) );
    exit ( 1 );
  }

  snd_pcm_writei( handle, buffer, duration );

} // end of PlaySound

void SuffixStatement( char *Text )
{
	ExtractParameters( "SUFFIX", "sI", Text );

	SetVariable( Par1, Suffix( atoi( Par2 ) ) );

}	// end of SuffixStatement

char *Suffix( int Number )
{
	// return a pointer to st, nd, rd or th
	// according to the number passed
	// this allows numbers like 1st, 2nd, 21st, 3rd, 23rd
	// etc to be printed out correctly

	static char *Ptr;

	while( Number > 1000 )
			Number -= 1000;

	while( Number > 100 )
			Number -= 100;

	if ( ( Number > 10 ) && ( Number < 20 ) ) {
		Ptr="th";
		return ( Ptr );
	}

	while( Number > 10 )
			Number -= 10;

	switch( Number ) {
			case  1: Ptr="st";
					break;

			case  2: Ptr="nd";
					break;

			case  3: Ptr="rd";
					break;

			default: Ptr="th";
					break;
	}

	return ( Ptr );

}	// end of suffix

void BlankString( char *String, char Set, int Count )
{
	memset( String, Set, ( size_t ) Count );

}	// end of Blankstring

void TickerStatement( char *Text )
{
	ExtractParameters( "TICKER", "IISII", Text );

	if ( strlen( Par3 ) == 0 )
			return;

	iScratch1=atol( Par1 );
	iScratch2=atol( Par2 );
	iScratch3=atol( Par4 );
	iScratch4=atol( Par5 );

	iScratch3 = ( ( iScratch3+iScratch2 ) > 80L ) ? 80L-iScratch2 : iScratch3;
	iScratch3 = ( iScratch3 < 1L ) ? ( long ) strlen( Par3 ) : iScratch3;

	iScratch4 = ( iScratch4 < 100L ) ? 100L : iScratch4;
	iScratch4 = ( iScratch4 > 1000L ) ? 1000L : iScratch4;

	ScrollMessage( Par3, ( int ) iScratch1, ( int ) iScratch2, iScratch3, iScratch4 );

}	// end of TickerStatement

void ScrollMessage( char *Message, int Xpos, int Ypos, int Width, int Delay )
{
	int KeyPress=0, c=0, i=0, Letter, Length=strlen( Message );
	int Pos=Width-1, First=0;

	struct timespec sleepValue, returnValue;
	sleepValue.tv_sec = 0;
	sleepValue.tv_nsec = INTERVAL_MS;

	do {
		GotoScreenXY( Xpos, Ypos );

		for ( i=0; i<Pos; i++ )
				putchar(' ');

		for ( i=Pos; i<Width; i++ ) {
			Letter=i-Pos;

			if ( Message[ First+Letter ] == '\x00')
					break;
			else
					putchar( Message[ First+Letter ] );
		}

		if ( Pos+( Length-First ) < Width ) {
			for ( c=0, i=Pos+( Length-First ); i<Width; c++, i++ ) {
				putchar( Message[ c ] );
				c = ( ( c+1 ) == Length ) ? ( -1 ) : c;
			}
		}

		if ( Pos > 0 ) {
			--Pos;
		}
		else {
			if ( Message[ First ] == '\x00')
					First=0;
			else
					++First;
		}

		if ( kbhit() ) {
			KeyPress=getchar();
			KeyPress=( KeyPress == '\x00') ? getchar() : KeyPress;
			break;
		}

		// sleep for a short duration to make the scrolling visually smooth
		nanosleep( &sleepValue, &returnValue );

	} while ( TRUE == TRUE );

}	// end of ScrollMessage

void GetXYStatement( char *Text )
{
	ExtractParameters( "GETXY", "ii", Text );

	sprintf( Scratch1, "%d", get_x() );
	SetVariable( Par1, Scratch1 );

	sprintf( Scratch1, "%d", get_y() );
	SetVariable( Par2, Scratch1 );

}	// end of GetXYStatement

void DayNameStatement( char *Text )
{
	ExtractParameters( "DAYNAME", "sI", Text );

	iScratch1=atol( Par2 );

	iScratch1=( iScratch1 > 6L ) ? 6L : iScratch1;
	iScratch1=( iScratch1 < 0L ) ? 0L : iScratch1;

	SetVariable( Par1, DayNames[ ( int ) iScratch1 ] );

}	// end of DayNameStatement

void MonthNameStatement( char *Text )
{
	ExtractParameters( "MONTHNAME", "sI", Text );

	iScratch1=atol( Par2 );

	iScratch1=( iScratch1 > 12L ) ? 12L : iScratch1;
	iScratch1=( iScratch1 < 1L ) ? 1L : iScratch1;

	SetVariable( Par1, MonthNames[ ( int ) iScratch1 ] );

}	// end of MonthNameStatement

void GetSystemTime( void )
{
	time_t t;
	struct tm *tmp;

	// get the time and date
	t = time( NULL );
	tmp = localtime( &t );

	sMin  = tmp->tm_min;
	sHour = tmp->tm_hour;
	sSec  = tmp->tm_sec;

}	// end of GetSystemTime

void GetSystemDate( void )
{
	time_t t;
	struct tm *tmp;

	// get the time and date
	t = time( NULL );
	tmp = localtime( &t );

	dYear  = tmp->tm_year;
	dMonth = tmp->tm_mon;
	dDay   = tmp->tm_mday;

	// adjust dDay to correct year
	dYear += 1900;

	// adjust month to correct month 1 - 12
	dMonth++;

}	// end of GetSystemDate

void HourStatement( char *Text )
{
	ExtractParameters( "HOUR", "i", Text );

	GetSystemTime();

	sprintf( Scratch1, "%d", sHour );

	SetVariable( Par1, Scratch1 );

}	// end of HourStatement

void MinuteStatement( char *Text )
{
	ExtractParameters( "MINUTE", "i", Text );

	GetSystemTime();

	sprintf( Scratch1, "%d", sMin );

	SetVariable( Par1, Scratch1 );

}	// end of MinuteStatement

void SecondStatement( char *Text )
{
	ExtractParameters( "SECOND", "i", Text );

	GetSystemTime();

	sprintf( Scratch1, "%d", sSec );

	SetVariable( Par1, Scratch1 );

}	// end of SecondStatement

void HundredStatement( char *Text )
{
	ExtractParameters( "HUNDRED", "i", Text );

	GetSystemTime();

	sprintf( Scratch1, "%d", sHund );

	SetVariable( Par1, Scratch1 );

}	// end of HundredStatement

void YearStatement( char *Text )
{
	ExtractParameters( "YEAR", "i", Text );

	GetSystemDate();

	sprintf( Scratch1, "%d", dYear );

	SetVariable( Par1, Scratch1 );

}	// end of YearStatement

void MonthStatement( char *Text )
{
	ExtractParameters( "MONTH", "i", Text );

	GetSystemDate();

	sprintf( Scratch1, "%d", dMonth );

	SetVariable( Par1, Scratch1 );

}	// end of MonthStatement

void DayStatement( char *Text )
{
	ExtractParameters( "DAY", "i", Text );

	GetSystemDate();

	sprintf( Scratch1, "%d", dDay );

	SetVariable( Par1, Scratch1 );

}	// end of DayStatement

void DebugStatement( char *Text )
{
	int Response;
	static char *Pars[]={
		"OFF",
	 	"FULL",
		"ERROR",
		"TERMINATE",
		NULL
	};

	Function="DEBUG";
	Syntax="must be followed by OFF, FULL, ERROR or TERMINATE";

	RemoveCommandWord( Text, Function );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, Syntax );

	for ( Response=0; Response < 4; Response++ ) {
		if ( strcmp( Text, Pars[ Response ] ) == 0 )
				break;
	}

	if ( Response > 3 )
			ReportError( SYNTAX, UNRECOG_OPTION,
					"%s %s\n> \"%s\" is an invalid option",
							Function, Syntax, Text );
	else
			DEBUG=Response;

}	// end of DebugStatement

void DoStatement( char *Text )
{
	RemoveCommandWord( Text, "DO" );

	if ( ( strlen( Text ) != 0 ) && ( Text[ 0 ] != COMMENT ) )
			ReportError( SYNTAX, BAD_DO_LOG, NULL );

	if ( ++DO_DEPTH == MAX_DO_DEPTH )
			ReportError( GENERAL, DO_TOO_DEEP, "DO nesting is limited to a depth of %d DO/UNTIL loops", MAX_DO_DEPTH );

	DEEPEST_DO = ( DO_DEPTH > DEEPEST_DO ) ? DO_DEPTH : DEEPEST_DO;

	// allocate memory for new DO statement
	dTable[ DO_DEPTH ] = ( Do * ) malloc( sizeof( Do ) );

	// flag an error if no memory left
	if ( dTable[ DO_DEPTH ] == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for DoElement[ %d ]", DO_DEPTH );

	// record the line for the until statement to loop back to
	dTable[ DO_DEPTH ]->LoopBackLine=CURRENT_LINE;

}	// end of DoStatement

int UntilStatement( char *Text )
{
	int Response;

	RemoveCommandWord( Text, "UNTIL" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_UNTIL_CONDITION, NULL );

	if ( ( Text[ 0 ] != LEFT_PAREN ) || ( Lastch( Text ) != RIGHT_PAREN ) )
			ReportError( SYNTAX, NO_UNTIL_BRACKETS, NULL );

	if ( strcmp( dTable[ DO_DEPTH ]->UntilExp, "1st Time" ) == 0 ) {
		dTable[ DO_DEPTH ]->UntilExp = strdup( Text );
		if ( dTable[ DO_DEPTH ]->UntilExp == NULL ) {
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for DoElement[ %d ]->UntilExp", DO_DEPTH );
		}
	}

	ReplaceVariables( Text, CALCULATE );
	Response=LogicResolve( Text );

	if ( Response == TRUE ) {

		free( ( void * ) dTable[ DO_DEPTH ] );
		dTable[ DO_DEPTH ]=NULL;
		DO_DEPTH--;
		return ( CURRENT_LINE );
	}

	return ( dTable[ DO_DEPTH ]->LoopBackLine );

}	// end of UntilStatement

void ReportError( int ErrorType, int ErrorNumber, char *ErrorText, ... )
{
	char ErrorBuffer[ 250 ];
	va_list argptr;

	printf( "+++\n%s\n%s %s %s\n", Program, Version, __DATE__, __TIME__ );

	printf( "%s Error %03d: %s\n",
			( ErrorType == SYNTAX ) ? "Syntax" : "General",
			ErrorNumber, ErrorMessage[ ErrorNumber ] );

	if ( ErrorText != NULL ) {
		va_start( argptr, ErrorText );
		vsprintf( ErrorBuffer, ErrorText, argptr );
		va_end( argptr );
		printf( "> %s\n", ErrorBuffer );
	}

	if ( CURRENT_LINE != 0 )
			printf( "[ %03d ] %s\n", CURRENT_LINE, Source[ CURRENT_LINE ] );

	if ( ( DEBUG == FULL ) || ( DEBUG == ERROR ) )
			MemoryDump();

	end_charm();

	exit( 1 );

}	// end of ReportError

void FileStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FILE", "x", Text );

	// no Files in the table yet
	if ( FileStart == NULL ) {
		fPtr = ( File * ) malloc( sizeof( File ) );
		if ( fPtr == NULL )
				ReportError( GENERAL, NO_MEMORY,
						"Can't allocate memory for File: %s", Par1 );

		// fill in the data fields of the File
		if ( ( fPtr->Name = ( char * ) strdup( Par1 ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY,
						"Can't allocate memory for File: %s", Par1 );

		fPtr->Open=FALSE;
		fPtr->Handle=NULL;
		fPtr->Prev=NULL;
		fPtr->Next=NULL;

		// set this File as the start of the Table
		FileStart=fPtr;
	}
	else {
		// Files exist - check we haven't already used the File name
		if ( fLookUp( Par1 ) != NULL )
				ReportError( SYNTAX, DUPLICATE_FILE,
				"A File with the name \"%s\" already exists", Par1 );

		// find the last in the list
		for ( fPtr=FileStart; fPtr != NULL; fPtr = fPtr->Next )
				LastFile=fPtr;

		fPtr = ( File * ) malloc( sizeof( File ) );
		if ( fPtr == NULL )
				ReportError( GENERAL, NO_MEMORY,
						"Can't allocate memory for File: %s", Par1 );

		// fill in the data fields of the File
		if ( ( fPtr->Name = ( char * ) strdup( Par1 ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY,
						"Can't allocate memory for File: %s", Par1 );

		fPtr->Open=FALSE;
		fPtr->Handle=NULL;
		fPtr->Prev=LastFile;
		fPtr->Next=NULL;

		// point the previous File to the new Table element
		LastFile->Next=fPtr;
	}

}	// end of FileStatement

File *fLookUp( char *fName )
{
	File *fPtr;

	for ( fPtr=FileStart; fPtr != NULL; fPtr = fPtr->Next ) {
		if ( strcasecmp( fName, fPtr->Name ) == 0 )
				return ( fPtr );
	}

	return ( NULL );

}	// end of fLookUp

void CloseStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "CLOSE", "x", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN,
			"Encountered an attempt to close %s before it has been opened",
			fPtr->Name );

	fPtr->Open=FALSE;

	fclose( fPtr->Handle );

}	// end of CloseStatement

void OpenStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "OPEN", "xS", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == TRUE )
			ReportError( SYNTAX, FILE_ALREADY_OPEN,
			"Encountered an attempt to re-open the file: %s", fPtr->Name );

	for ( iScratch1=0L; Par2[ ( int ) iScratch1 ] != EOS; iScratch1++ ) {

		switch ( Par2[ ( int ) iScratch1 ] ) {
			case 'w' :
			case 'r' :
			case 'b' :
			case 'a' :
			case 't' :
			case '+' : break;

			  default: ReportError( SYNTAX, INVALID_FMODE,
							"Unknown file mode symbol \"%c\"",
							Par2[ ( int ) iScratch1 ] );
		 }
	}

	fPtr->Handle=fopen( fPtr->Name, Par2 );

	if ( fPtr->Handle == NULL )
			ReportError( SYNTAX, OPEN_FAILED,
					"File open failed: %s, Mode: %s", fPtr->Name, Par2 );

	fPtr->Open=TRUE;

}	// end of OpenStatement

void FgetsStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FGETS", "xsIi", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN, "Encountered an attempt to access %s before it has been opened", fPtr->Name );

	if ( fgets( Scratch1, atoi( Par3 ), fPtr->Handle ) != NULL ) {
		SetVariable( Par2, Scratch1 );
		sprintf( Scratch2, "%d", ( int ) strlen( Scratch1 ) );
		SetVariable( Par4, Scratch2 );
	}
	else {
		strcpy( Scratch1, "" );
      SetVariable( Par2, Scratch1 );
      strcpy( Scratch1, "-1" );
		SetVariable( Par4, Scratch1 );
	}

}	// end of FgetsStatement

void KillStatement( char *Text )
{
	File *fPtr, *pPtr;

	ExtractParameters( "FKILL", "x", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == TRUE )
			fclose( fPtr->Handle );

	if ( fPtr != FileStart ) {
		pPtr=fPtr->Prev;
		pPtr->Next=fPtr->Next;
	}

	if ( FileStart == fPtr )
			FileStart=fPtr->Next;

	if ( fPtr->Next != NULL ) {
		pPtr=fPtr->Next;
		pPtr->Prev=fPtr->Prev;
	}

	fPtr->Open=FALSE;
	fPtr->Next=NULL;
	fPtr->Prev=NULL;
	fPtr->Handle=NULL;

	free( ( void * ) fPtr->Name );
	free( ( void * ) fPtr );

}	// end of KillStatement

void FgetposStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FGETPOS", "xi", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN,
			"Encountered an attempt to access %s before it has been opened",
			fPtr->Name );

	fgetpos( fPtr->Handle, ( fpos_t * ) &iScratch1 );
	sprintf( Scratch1, "%ld", iScratch1 );
	SetVariable( Par2, Scratch1 );

}	// end of FgetposStatement

void FsetposStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FSETPOS", "xI", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN,
			"Encountered an attempt to access %s before it has been opened",
			fPtr->Name );

	iScratch1=atol( Par2 );

	iScratch2=( long ) fsetpos( fPtr->Handle, ( fpos_t * ) &iScratch1 );

}	// end of FsetposStatement

void FlushStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FLUSH", "x", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN,
			"Encountered an attempt to access %s before it has been opened",
			fPtr->Name );

	fflush( fPtr->Handle );

}	// end of FlushStatement

void FlushallStatement( char *Text )
{
	File *fPtr;

	RemoveCommandWord( Text, "FLUSHALL" );

	if ( ( strlen( Text ) != 0 ) && ( Text[ 0 ] != COMMENT ) )
			ReportError( SYNTAX, BAD_FLUSHALL, NULL );

	for ( fPtr = FileStart; fPtr != NULL; fPtr = fPtr->Next )
		fflush( fPtr->Handle );

}	// end of FlushallStatement

void CloseallStatement( char *Text )
{
	File *fPtr;

	RemoveCommandWord( Text, "CLOSEALL" );

	if ( ( strlen( Text ) != 0 ) && ( Text[ 0 ] != COMMENT ) )
			ReportError( SYNTAX, BAD_FLUSHALL, NULL );

	for ( fPtr = FileStart; fPtr != NULL; fPtr = fPtr->Next ) {
		fPtr->Open=FALSE;
		fclose( fPtr->Handle );
	}

}	// end of CloseallStatement

void UnlinkStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "UNLINK", "x", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr->Open == TRUE )
			ReportError( SYNTAX, FILE_STILL_OPEN, "Encountered an attempt to delete %s before it has been closed", Par1 );

	if ( unlink( Par1 ) != 0 )
			ReportError( SYNTAX, CANNOT_DELETE, "Couldn't delete the %s file", Par1 );

}	// end of UnlinkStatement

int getch( void )
{
    struct termios oldattr, newattr;
    int ch;

    // reads from keypress, doesn't echo

    tcgetattr( STDIN_FILENO, &oldattr );

    newattr = oldattr;

    newattr.c_lflag &= ~( ICANON | ECHO );

    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );

    ch = getchar();

    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );

    return ch;

}	// end of getch

int ForStatement( char *Text )
{
	char *Ptr1, *Ptr2;
	static char Assign[ 50 ]="", Cease[ 50 ]="", Step[ 50 ]="";
	double Answer;
	int i, StartScanLevel;

	vList *vPtr;

	// take off the FOR word
	RemoveCommandWord( Text, "FOR" );

	// error if that's all there was
	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_FOR_CONDITIONAL, NULL );

	// otherwise find the position of the TO clause
	Ptr2=strstr( Text, " TO " );

	// and scoop everything before that into the Buffer
	for ( i=0, Ptr1=Text; Ptr1 < Ptr2; Ptr1++, i++ )
			Assign[ i ] = *Ptr1;

	// cap the buffer
	Assign[ i ]=EOS;

	Text = Ptr2;

	// take off the TO word
	RemoveLeadingWhitespace( Text );
	RemoveCommandWord( Text, "TO" );

	// error if that's all there was
	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_FOR_TO_CLAUSE, NULL );

	// else find out the position ( if any ) of the STEP clause
	Ptr2=strstr( Text, "STEP" );

	if ( Ptr2 != NULL ) {
		// scoop everything up to the STEP statement into the Buffer
		for ( i=0, Ptr1=Text; Ptr1 < Ptr2; Ptr1++, i++ )
				Cease[ i ] = *Ptr1;

		// cap the buffer
		Cease[ i ]=EOS;

		Text = Ptr2;

		// now get the step clause of the for statement
		RemoveLeadingWhitespace( Text );
		RemoveCommandWord( Text, "STEP" );

		// error if that's all there was
		if ( strlen( Text ) == 0 )
				ReportError( SYNTAX, NO_STEP_VALUE, NULL );

		for ( i=0; Text[ i ] != EOS; i++ )
				Step[ i ] = Text[ i ];

		// cap the buffer
		Step[ i ]=EOS;
	}
	else {
		// scoop everything up to the end of the string into the Buffer
		for ( i=0, Ptr1=Text; *Ptr1 != EOS; Ptr1++, i++ )
				Cease[ i ] = *Ptr1;

		// cap the buffer
		Cease[ i ]=EOS;
	}

	if ( ++FOR_DEPTH == MAX_FOR_DEPTH )
			ReportError( SYNTAX, FOR_TOO_DEEP,
				"FOR loop nesting is limited to a depth of %d FOR loops",
				MAX_FOR_DEPTH );

	// is this the deepest we have been in the for table
	DEEPEST_FOR = ( FOR_DEPTH > DEEPEST_FOR ) ? FOR_DEPTH : DEEPEST_FOR;

	vPtr = LookUp( PullFirstWord( Assign ), ALL );

	if ( vPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_VARIABLE, NULL );
	else if ( vPtr->Type == 's')
			ReportError( SYNTAX, BAD_FOR_VARIABLE, NULL );

	CheckForVariableOperations( Assign, PROC_CALLS, SET );

	ReplaceVariables( Cease, CALCULATE );

	if ( strpbrk( Cease, "+-/^*" ) != NULL ) {
			strcpy( Express, Cease );
			GetExpression( &Answer );
			sprintf( Cease, "%f", Answer );
	}

	if ( strlen( Step ) != 0 ) {
		ReplaceVariables( Step, CALCULATE );

		if ( strpbrk( Step, "+-/^*" ) != NULL ) {
				strcpy( Express, Step );
				GetExpression( &Answer );
				sprintf( Step, "%f", Answer );
		}
	}
	else
			sprintf( Step, "%f", 1.0 );

	fTable[ FOR_DEPTH ]=( For * ) malloc( sizeof( For ) );

	if ( fTable[ FOR_DEPTH ] == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for ForElement[ %d ]", FOR_DEPTH );

	fTable[ FOR_DEPTH ]->vName = strdup( vPtr->vName );

	if ( fTable[ FOR_DEPTH ]->vName == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for ForElement[ %d ]->vName",
					FOR_DEPTH );

	fTable[ FOR_DEPTH ]->Cease=( float ) atof( Cease );
	fTable[ FOR_DEPTH ]->Step= ( float ) atof( Step );

	if ( fTable[ FOR_DEPTH ]->Step == 0.0 )
			fTable[ FOR_DEPTH ]->Step = 1.0;

	fTable[ FOR_DEPTH ]->LoopStartLine=CURRENT_LINE;

	if ( ForComparison( vPtr, fTable[ FOR_DEPTH ]->Cease, fTable[ FOR_DEPTH ]->Step )
			 != TRUE ) {
		DEEPEST_FOR--;
		free( ( void * ) fTable[ FOR_DEPTH ]->vName );
		free( ( void * ) fTable[ FOR_DEPTH ] );
		fTable[ FOR_DEPTH ]=NULL;

		StartScanLevel=FOR_DEPTH;

		for ( i=CURRENT_LINE+1; i < LINES_READ; i++ ) {
			if ( FOR_DEPTH < StartScanLevel )
					break;
			if ( strcmp( "FOR", PullFirstWord( Source[ i ] ) ) == 0 )
					FOR_DEPTH++;
			else if ( strcmp( "ENDFOR", PullFirstWord( Source[ i ] ) ) == 0 )
					FOR_DEPTH--;
			else
					continue;
		}
		return ( i-1 );
	}

	return ( CURRENT_LINE );

}	// end of ForStatement

int ForComparison( vList *vPtr, float Cease, int Step )
{
	int Response;

	if ( Step >= 1 ) {
		switch ( vPtr->Type ) {
			case  'i': Response = ( ( atol( vPtr->vValue ) ) > ( ( long ) Cease ) )
								? FALSE : TRUE;
				   	   break;

			case  'f': Response = ( atof( vPtr->vValue ) > Cease ) ? FALSE : TRUE;
				   	   break;

		  	  default: break;
		}
	}
	else {
		switch ( vPtr->Type ) {
			case  'i': Response = ( ( atol( vPtr->vValue ) ) < ( ( long ) Cease ) )
								? FALSE : TRUE;
				   	   break;

			case  'f': Response = ( atof( vPtr->vValue ) < Cease )
								? FALSE: TRUE;
				   	   break;

		  	  default: break;
		}
	}

	return ( Response );

}	// end of ForComparison

int EndForStatement( char *Text )
{
	vList *vPtr;
	char Increase[ 25 ];
	int i;

	if ( FOR_DEPTH == 0 )
			ReportError( SYNTAX, MISPLACED_ENDFOR, NULL );

	// check we haven't got any unwanted parameters
	RemoveCommandWord( Text, "ENDFOR" );

	// error if there are parameters
	if ( strlen( Text ) != 0 )
			ReportError( SYNTAX, ENDFOR_WITH_PARS, NULL );

	// find the variable in question - we know it already exists
	vPtr=LookUp( fTable[ FOR_DEPTH ]->vName, ALL );

	// check whether the loop condition is still true
	if ( ForComparison( vPtr, fTable[ FOR_DEPTH ]->Cease, fTable[ FOR_DEPTH ]->Step )
			 == TRUE ) {

		// adjust the variable and loop back to the start of the loop
		sprintf( Increase, "%s+ ( %f )", vPtr->vValue, fTable[ FOR_DEPTH ]->Step );

		SetVariable( vPtr->vName, Increase );

		if ( ForComparison( vPtr, fTable[ FOR_DEPTH ]->Cease,
				fTable[ FOR_DEPTH ]->Step ) == TRUE ) {

			return ( fTable[ FOR_DEPTH ]->LoopStartLine );

		}
		else {
			// exit from loop and clear up for nesting arrays
			for ( i=FOR_DEPTH; i<= DEEPEST_FOR; i++ ) {
				if ( fTable[ i ] != NULL ) {
					free( ( void * ) fTable[ i ]->vName );
					free( ( void * ) fTable[ i ] );
					fTable[ i ]=NULL;
				}
			}
			FOR_DEPTH--;
			return ( CURRENT_LINE );
		}
	}
	else {
		// exit from loop and clear up for nesting arrays
		for ( i=FOR_DEPTH; i<= DEEPEST_FOR; i++ ) {
			if ( fTable[ i ] != NULL ) {
				free( ( void * ) fTable[ i ]->vName );
				free( ( void * ) fTable[ i ] );
				fTable[ i ]=NULL;
			}
		}
		FOR_DEPTH--;
		return ( CURRENT_LINE );
	}

}	// end of EndForStatement

void GetvalStatement( char *Text )
{
	ExtractParameters( "GETVAL", "i", Text );

	iScratch1=( int ) getch();
	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of GetvalStatement

void GetcharStatement( char *Text )
{
	ExtractParameters( "GETCHAR", "s", Text );

	sprintf( Scratch1, "%c", getch() );

	SetVariable( Par1, Scratch1 );

}	// end of GetcharStatement

void PutvalStatement( char *Text )
{
	ExtractParameters( "PUTVAL", "I", Text );

	iScratch1=atol( Par1 );

	iScratch1=( iScratch1 > 255L ) ? 255L : iScratch1;
	iScratch1=( iScratch1 < 0L ) ? 0L : iScratch1;

	printf( "%c", ( int ) iScratch1 );

}	// end of PutvalStatement

void PutcharStatement( char *Text )
{
	ExtractParameters( "PUTCHAR", "S", Text );

	iScratch1=( long ) *Par1;

	iScratch1=( iScratch1 > 255L ) ? 255L : iScratch1;
	iScratch1=( iScratch1 < 0L ) ? 0L : iScratch1;

	printf( "%c", ( int ) iScratch1 );

}	// end of PutcharStatement

void InputStatement( char *Text )
{
	Function="INPUT";
	Syntax="must be followed by an IntVar%, a FloatVar or a StringVar$";

	RemoveCommandWord( Text, Function );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, Syntax );

	if ( *Text == QUOTE )
			ReportError( SYNTAX, MISPLACED_STR, "%s %s", Function, Syntax );

	if ( strpbrk( Text, "+-/^*" ) != NULL )
			ReportError( SYNTAX, MISPLACED_EXP, "%s %s", Function, Syntax );

	fgets( Scratch1, 150, stdin );

	SetVariable( Text, Scratch1 );

}	// end of InputStatement

void EditStatement( char *Text )
{
	vList *vPtr;
	Function="EDIT";
	Syntax="must be followed by an IntVar%, a FloatVar or a StringVar$";

	RemoveCommandWord( Text, Function );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, Syntax );

	if ( *Text == QUOTE )
			ReportError( SYNTAX, MISPLACED_STR, "%s %s", Function, Syntax );

	if ( strpbrk( Text, "+-/^*" ) != NULL )
			ReportError( SYNTAX, MISPLACED_EXP, "%s %s", Function, Syntax );

	vPtr=LookUp( Text, ALL );

	// not recognised - flag error
	if ( vPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_VARIABLE, "Symbol not found in the Variable Table: %s", Text );

	strcpy( Scratch1, vPtr->vValue );

	StringEdit( Scratch1 );

	if ( Scratch1[ 1 ] != '\x01')
			SetVariable( Text, Scratch1 );

}	// end of EditStatement

unsigned KeyPress( void )
{
	char ch;

	return ( ( ch = getch() ) != '\0') ? ch : 0x8000 | getch();

}   // end of KeyPress

void StringEdit( char *EditLine )
{
	int Keep = 0, i, j, Space, Redraw = 1, Offset = 0, InsertMode = 1, Line, Column;
	int MAX_LENGTH; //, EditCurTop, EditCurBottom;
	unsigned KeyStroke;
	char EditBuffer[ 256 ];

	Line = get_x();
	Column = get_y();

	memset( EditBuffer, 0x00, ( size_t ) 255 );
   strcpy( EditBuffer, EditLine );

	MAX_LENGTH=( 79 - Column );

    for ( ;; ) {

        if ( Redraw ) {
            cursor_off();
            Redraw ^= 1;
            move_cursor( Column, Line );
            printf( "%s ", EditBuffer );
        }

		move_cursor( Column+Offset, Line );

		if ( InsertMode )
				cursor_line();
		else
				cursor_block();

		cursor_on();

		KeyStroke=get_key();

      switch ( KeyStroke ) {
			// escape
         case KEY_ESCAPE:
         	sprintf( EditBuffer, "%c", 0x01 );
				Keep = 1;
            break;

			// enter
			case KEY_NEWLINE:
				Keep = 2;
				move_cursor( Column+strlen( EditBuffer ), Line );
				break;

			// Home
			case KEY_HOME:
				Offset = 0;
				break;

			// insert
			case KEY_INSERT:
				InsertMode ^= 1;
				break;

			// left arrow
			case KEY_LEFT:
				if ( Offset > 0 )
						Offset--;
				break;

			// right arrow
			case KEY_RIGHT:
				if ( Offset < strlen( EditBuffer ) )
						Offset++;
				break;

			// end
			case KEY_END:
				Offset=strlen( EditBuffer );
				break;

			// backspace
			case KEY_BACKSPACE:
				if ( Offset == 0 )
						break;
				else
						Offset--;

				// NB no break, drop through to delete function to erase the character

			case KEY_DELETE:
          	if ( Offset < strlen( EditBuffer ) ) {
          		Redraw ^= 1;
          		strcpy( &EditBuffer[ Offset ], &EditBuffer[ Offset+1 ] );
				}
				break;

			// ctrl left arrow
			case KEY_CTRL_LEFT:

				if ( Offset == 0 )
						break;

				// are we at End?
				i = ( Offset == strlen( EditBuffer ) ) ? Offset-1 : Offset;

				// is the previous character a space
				if ( isspace( EditBuffer[ i-1 ] ) )
						--i;

				// Keep going backwards until we hit non-space
				if ( isspace( EditBuffer[ i ] ) ) {
					while( isspace( EditBuffer[ i ] ) ) {
						i--;
						if ( i == 0 )
								break;
					}
				}

				j = i;
				Space = 0;

				// Keep going backwards until we hit space or Home
				for ( i = j; i >= 0; i-- ) {
					if ( isspace( EditBuffer[ i-1 ] ) ) {
						Offset=i;
						Space=1;
						break;
					}
				}

				Offset = ( Space == 1 ) ? i : 0;
				break;

		// ctrl right arrow
		case KEY_CTRL_RIGHT:
			if ( Offset == strlen( EditBuffer ) )
						break;

		   i = Offset;

			// Keep going forwards until we hit non-space
			if ( !isspace( EditBuffer[ i ] ) ) {
				while( !isspace( EditBuffer[ i ] ) ) {
					i++;

					if ( i == strlen( EditBuffer ) )
							break;
				}
			}

			j = i;
			Space = 0;

			// Keep going forwards until we hit non-space or End
			for ( i = j; i <= strlen( EditBuffer ); ++i ) {

				if ( !isspace( EditBuffer[ i ] ) ) {
					Offset = i;
					Space = 1;
					break;
				}
			}

			Offset = ( Space == 1 ) ? i : strlen( EditBuffer );
			break;

		// use the character?
		default:
			if ( strlen( EditBuffer ) > MAX_LENGTH )
					printf("\a");
			else if ( KeyStroke < 32 || KeyStroke >= 127 )
					printf("\a");
			else {

				if ( Offset == strlen( EditBuffer ) ) {
					EditBuffer[ Offset++ ] = KeyStroke;
					EditBuffer[ Offset ]='\0';
					Redraw ^= 1;
				}
				else if ( InsertMode == 0 ) {
					EditBuffer[ Offset++ ] = KeyStroke;
					Redraw ^= 1;
				}
				else {
					memmove( EditBuffer+Offset+1, EditBuffer + Offset, strlen( &EditBuffer[ Offset ] )+1 );
					EditBuffer[ Offset++ ] = KeyStroke;
					Redraw ^= 1;
				}
			}
			break;

		} // switch

		// escape or enter were pressed
		if ( Keep != 0 )
				break;
	}	// if

	// if enter was pressed, save the edited line
	if ( Keep == 2 )
			strcpy( EditLine, EditBuffer );

}   // end of StringEdit

void GlobalStatement( char *Text )
{
	RemoveCommandWord( Text, "GLOBAL" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, GLOBAL_NO_VAR, NULL );
	else
			CheckForVariableOperations( Text, GLOBAL, FIX );

}	// end of GlobalStatement

void LocalStatement( char *Text )
{
	RemoveCommandWord( Text, "LOCAL" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, LOCAL_NO_VAR, NULL );
	else
			CheckForVariableOperations( Text, PROC_CALLS, FIX );

}	// end of LocalStatement

void ConstantStatement( char *Text )
{
	RemoveCommandWord( Text, "CONSTANT" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, CONSTANT_NO_VAR, NULL );
	else
			CheckForVariableOperations( Text, CONSTANT, FIX );

}	// end of ConstantStatement

int GotoStatement( char *Text )
{
	Label *Lptr;

	Function="GOTO";
	Syntax="must be followed by a valid label name";

	RemoveCommandWord( Text, Function );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, Syntax );

	Lptr=FindLabel( Text );

	if ( Lptr == NULL )
			ReportError( SYNTAX, UNKNOWN_LABEL, NULL );

	if ( IF_NESTING != 0 ) {
		if ( IF_NESTING > 1 )
				ReportError( GENERAL, GOTO_OUT_IF, "You can only GOTO out of non-nested IF statements" );

		if ( IF_NEST[ IF_NESTING ] == IF_WORKING )
				IF_NEST[ IF_NESTING-- ] = IF_DONE;
	}

	return ( Lptr->Line );

}	// end of GotoStatement

void IfStatement( char *Text )
{
	int Response;

	RemoveCommandWord( Text, "IF" );

	if ( ++IF_NESTING == MAX_IF_DEPTH )
			ReportError( GENERAL, IF_TOO_DEEP,
					"IF nesting is limited to a depth of %d IF/ENDIF loops",
					MAX_IF_DEPTH );

	DEEPEST_IF = ( IF_NESTING > DEEPEST_IF ) ? IF_NESTING : DEEPEST_IF;

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_IF_CONDITIONAL, NULL );

	if ( ( Text[ 0 ] != LEFT_PAREN ) || ( Lastch( Text ) != RIGHT_PAREN ) )
			ReportError( SYNTAX, NO_IF_BRACKETS, NULL );

	ReplaceVariables( Text, CALCULATE );
	Response=LogicResolve( Text );

	if ( Response == TRUE ) {
		SEEK_NEST[ IF_NESTING ]=IF_NESTING;
		IF_NEST[ IF_NESTING ]=IF_WORKING;
	}
	else {
		IF_NEST[ IF_NESTING ]=IF_SEEK;
		SEEK_NEST[ IF_NESTING ]=IF_NESTING;
	}

}	// end of IfStatement

void ElifStatement( char *Text )
{
	int Response;

	if ( IF_NEST[ IF_NESTING ] == IF_WORKING ) {
		IF_NEST[ IF_NESTING ] = IF_DONE;
		return;
	}

	RemoveCommandWord( Text, "ELIF" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_IF_CONDITIONAL, NULL );

	if ( ( Text[ 0 ] != LEFT_PAREN ) || ( Lastch( Text ) != RIGHT_PAREN ) )
			ReportError( SYNTAX, NO_IF_BRACKETS, NULL );

	ReplaceVariables( Text, CALCULATE );
	Response=LogicResolve( Text );

	if ( Response == TRUE ) {
		SEEK_NEST[ IF_NESTING ]=IF_NESTING;
		IF_NEST[ IF_NESTING ]=IF_WORKING;
	}
	else {
		IF_NEST[ IF_NESTING ]=IF_SEEK;
		SEEK_NEST[ IF_NESTING ]=IF_NESTING;
	}

}	// end of ElifStatement

void ElseStatement( char *Text )
{
	if ( IF_NEST[ IF_NESTING ] == IF_WORKING ) {
		IF_NEST[ IF_NESTING ]=IF_DONE;
		return;
	}

	RemoveCommandWord( Text, "ELSE" );

	if ( ( strlen( Text ) != 0 ) && ( Text[ 0 ] != COMMENT ) )
			ReportError( SYNTAX, BAD_ELSE_LOG, NULL );

	SEEK_NEST[ IF_NESTING ]=IF_NESTING;
	IF_NEST[ IF_NESTING ]=IF_WORKING;

}	// end of ElseStatement

void EndifStatement( void )
{
	if ( ( IF_NESTING ) <= 0 )
			ReportError( SYNTAX, BAD_ENDIF, NULL );

	IF_NEST[ IF_NESTING-- ]=NOT_IN_IF;
	SEEK_NEST[ IF_NESTING ]=IF_NESTING;

}	// end of EndifStatement

void InterpretSourceFile( void )
{
	static char CurrentLine[ 150 ]="";
	char szMainLine[] = "PROC Main";
	int mCommand, Line=0, Found=FALSE, StopRequest=FALSE;
	Proc *ProcPtr;
	vList *vPtr;

	ProcPtr=FindProc( "Main" );

	if ( ProcPtr == NULL )
			ReportError( GENERAL, NO_MAIN, "All source files must contain a procedure called Main" );
	else
			CURRENT_LINE=ProcStatement( szMainLine, 0 );

	// line by line through the script file in memory
	for ( Line=CURRENT_LINE; PROC_RETURNS != 0; Line++ ) {

		CURRENT_LINE=Line;

		Found=FALSE;

		// if this line contains nothing much ignore it
		if ( strlen( Source[ Line ] ) <= 1 )
				continue;

		// don't do anything with comment and label lines
		if ( ( *( Source[ Line ] ) == COMMENT ) || ( *( Source[ Line ] ) == COLON ) )
                continue;
		else
				strcpy( CurrentLine, Source[ Line ] );

		// in any IF loops?
		if ( IF_NESTING != 0 ) {
			if ( IF_NEST[ IF_NESTING ] == IF_SEEK ) {
				if ( GetCommandWord( CurrentLine, "IF" ) == 0 ) {
					SEEK_NEST[ IF_NESTING ]++;
					continue;
				}
				else if ( GetCommandWord( CurrentLine, "ELIF" ) == 0 ) {
					if ( SEEK_NEST[ IF_NESTING ] != IF_NESTING )
							continue;
				}
				else if ( GetCommandWord( CurrentLine, "ELSE" ) == 0 ) {
					if ( SEEK_NEST[ IF_NESTING ] != IF_NESTING )
							continue;
				}
				else if ( GetCommandWord( CurrentLine, "ENDIF" ) == 0 ) {
					if ( SEEK_NEST[ IF_NESTING ] != IF_NESTING ) {
						SEEK_NEST[ IF_NESTING ]--;
						continue;
					}
				}
				else
						continue;
			}
			else if ( IF_NEST[ IF_NESTING ] == IF_DONE ) {
				if ( GetCommandWord( CurrentLine, "IF" ) == 0 ) {
					SEEK_NEST[ IF_NESTING ]++;
					continue;
				}
				else if ( GetCommandWord( CurrentLine, "ENDIF" ) == 0 ) {
					if ( SEEK_NEST[ IF_NESTING ] != IF_NESTING ) {
						continue;
					}
				}
				else
						continue;
			}
		}

		// reset the found flag and the command index counter
		mCommand=( -1 );

		// does the line contain a valid command?
		while ( ( Command[ ++mCommand ] != NULL ) ) {
			if ( GetCommandWord( PullFirstWord( CurrentLine ), Command[ mCommand ] ) == 0 ) {
				Found=TRUE;
				break;
			}
		}

		// otherwise see if it is an existing variable
		 if ( Found == FALSE ) {

			vPtr=LookUp( PullFirstWord( CurrentLine ), ALL );

			if ( vPtr == NULL ) {
				// are they calling a procedure name without the PROC?
				ProcPtr=FindProc( PullFirstWord( CurrentLine ) );

				if ( ProcPtr != NULL ) {
					// procedure call - add the PROC reserved word
					sprintf( CurrentLine, "PROC %s", Source[ Line ] );
					Found=TRUE;
					mCommand=11;
				}
				else
						ReportError( SYNTAX, UNKNOWN_VARIABLE, NULL );
			}

		 	if ( Found == FALSE ) {
				if ( CheckForVariableOperations( CurrentLine, PROC_CALLS, SET ) == TRUE )
						continue;
			}
		}

		// do something according to the detected command
		switch ( mCommand ) {

			// cls
			case rCLS:
				CLS;
				break;

			// beep
			case rBEEP:
				putchar( BELL );
				break;

			// print
			case rPRINT:
				PrintStatement( CurrentLine );
            break;

			// pause
			case rPAUSE:
				if ( getch() == 0 )
						getch();
				break;

			// if
			case rIF:
				IfStatement( CurrentLine );
				break;

			// elif
			case rELIF:
				ElifStatement( CurrentLine );
				break;

			// else
			case rELSE:
				ElseStatement( CurrentLine );
				break;

			// endif
			case rENDIF:
				EndifStatement();
				break;

			// goto
			case rGOTO:
				Line = GotoStatement( CurrentLine );
				break;

			// debug
			case rDEBUG:
				DebugStatement( CurrentLine );
				break;

			// defproc
			case rDEFPROC: break;

			// proc
			case rPROC:
				Line = ProcStatement( CurrentLine, 1 );
				break;

			// endproc and return
			case rENDPROC:
			case rRETURN:
				Line = EndprocStatement();
				break;

			// system
			case rSYSTEM:
				SystemStatement( CurrentLine );
				break;

			// global
			case rGLOBAL:
				GlobalStatement( CurrentLine );
				break;

			// local
			case rLOCAL:
				LocalStatement( CurrentLine );
				break;

			// for
			case rFOR:
				Line = ForStatement( CurrentLine );
				break;

			// endfor
			case rENDFOR:
				Line = EndForStatement( CurrentLine );
				break;

			// constant
			case rCONSTANT:
				ConstantStatement( CurrentLine );
				break;

			// decimal
			case rDECIMAL:
				DecimalStatement( CurrentLine );
				break;

			// at
			case rAT:
				AtStatement( CurrentLine );
				break;

			// while
			case rWHILE:
				Line = WhileStatement( CurrentLine );
				break;

			// endwhile
			case rENDWHILE:
				Line = EndwhileStatement( CurrentLine );
				break;

			// do
			case rDO:
				DoStatement( CurrentLine );
				break;

			// until
			case rUNTIL:
				Line = UntilStatement( CurrentLine );
				break;

			// tone
			case rTONE:
				ToneStatement( CurrentLine );
				break;

			// stop
			case rSTOP:
				printf( "\nSTOPped at line: %d\n", Line );
				if ( ( DEBUG == FULL ) || ( DEBUG == TERMINATE ) )
						MemoryDump();
				StopRequest = TRUE;
				break;

			// strlen
			case rSTRLEN:
				StrlenStatement( CurrentLine );
				break;

			// dayname
			case rDAYNAME:
				DayNameStatement( CurrentLine );
				break;

			// cursor
			case rCURSOR:
				CursorStatement( CurrentLine );
				break;

			// monthname
			case rMONTHNAME:
				MonthNameStatement( CurrentLine );
				break;

			// hour
			case rHOUR:
				HourStatement( CurrentLine );
				break;

			// minute
			case rMINUTE:
				MinuteStatement( CurrentLine );
				break;

			// second
			case rSECOND:
				SecondStatement( CurrentLine );
				break;

			// hundred
			case rHUNDRED:
				HundredStatement( CurrentLine );
				break;

			// year
			case rYEAR:
				YearStatement( CurrentLine );
				break;

			// month
			case rMONTH:
				MonthStatement( CurrentLine );
				break;

			// day
			case rDAY:
				DayStatement( CurrentLine );
				break;

			// suffix
			case rSUFFIX:
				SuffixStatement( CurrentLine );
				break;

			// foreground
			case rFOREGROUND:
				ForegroundStatement( CurrentLine );
				break;

			// background
			case rBACKGROUND:
				BackgroundStatement( CurrentLine );
				break;

			// leftstr
			case rLEFTSTR:
				LeftStrStatement( CurrentLine );
				break;

			// rightstr
			case rRIGHTSTR:
				RightStrStatement( CurrentLine );
				break;

			// lowerstr
			case rLOWERSTR:
				LowerStrStatement( CurrentLine );
				break;

			// upperstr
			case rUPPERSTR:
				UpperStrStatement( CurrentLine );
				break;

			// copystr
			case rCOPYSTR:
				CopyStrStatement( CurrentLine );
				break;

			// strcat
			case rSTRCAT:
				StrCatStatement( CurrentLine );
				break;

			// intstr
			case rINTSTR:
				IntStrStatement( CurrentLine );
				break;

			// strint
			case rSTRINT:
				StrIntStatement( CurrentLine );
				break;

			// numstr
			case rNUMSTR:
				NumStrStatement( CurrentLine );
				break;

			// strnum
			case rSTRNUM:
				StrNumStatement( CurrentLine );
				break;

			// strloc
			case rSTRLOC:
				StrLocStatement( CurrentLine );
				break;

			// strcmp
			case rSTRCMP:
				StrCmpStatement( CurrentLine );
				break;

			// strcmpi
			case rSTRCMPI:
				StrCmpiStatement( CurrentLine );
				break;

			// ascval
			case rASCVAL:
				AscValStatement( CurrentLine );
				break;

			// strrev
			case rSTRREV:
				StrrevStatement( CurrentLine );
				break;

			// midstr
			case rMIDSTR:
				MidStrStatement( CurrentLine );
				break;

			// getval
			case rGETVAL:
				GetvalStatement( CurrentLine );
				break;

			// getchar
			case rGETCHAR:
				GetcharStatement( CurrentLine );
				break;

			// putval
			case rPUTVAL:
				PutvalStatement( CurrentLine );
				break;

			// putchar
			case rPUTCHAR:
				PutcharStatement( CurrentLine );
				break;

			// repstr
			case rREPSTR:
				RepstrStatement( CurrentLine );
				break;

			// abs
			case rABS:
				AbsStatement( CurrentLine );
				break;

			// fabs
			case rFABS:
				FabsStatement( CurrentLine );
				break;

			// rand
			case rRAND:
				RandStatement( CurrentLine );
				break;

			// frand
			case rFRAND:
				FrandStatement( CurrentLine );
				break;

			// ticker
			case rTICKER:
				TickerStatement( CurrentLine );
				break;

			// input
			case rINPUT:
				InputStatement( CurrentLine );
				break;

			// getxy
			case rGETXY:
				GetXYStatement( CurrentLine );
				break;

			// edit
			case rEDIT:
				EditStatement( CurrentLine );
				break;

			// getjd
			case rGETJD:
				GetjdStatement( CurrentLine );
				break;

			// jdate
			case rJDATE:
				JdateStatement( CurrentLine );
				break;

			// sin
			case rSIN:
				SinStatement( CurrentLine );
				break;

			// cos
			case rCOS:
				CosStatement( CurrentLine );
				break;

			// tan
			case rTAN:
				TanStatement( CurrentLine );
				break;

			// asin
			case rASIN:
				AsinStatement( CurrentLine );
				break;

			// acos
			case rACOS:
				AcosStatement( CurrentLine );
				break;

			// atan
			case rATAN:
				AtanStatement( CurrentLine );
				break;

			// rad
			case rRAD:
				RadStatement( CurrentLine );
				break;

			// deg
			case rDEG:
				DegStatement( CurrentLine );
				break;

			// exp
			case rEXP:
				ExpStatement( CurrentLine );
				break;

			// sqr
			case rSQR:
				SqrStatement( CurrentLine );
				break;

			// ln
			case rLN:
				LnStatement( CurrentLine );
				break;

			// log
			case rLOG:
				LogStatement( CurrentLine );
				break;

			// file
			case rFILE:
				FileStatement( CurrentLine );
				break;

			// close
			case rCLOSE:
				CloseStatement( CurrentLine );
				break;

			// open
			case rOPEN:
				OpenStatement( CurrentLine );
				break;

			// fprint
			case rFPRINT:
				FprintStatement( CurrentLine );
				break;

			// fgets
			case rFGETS:
				FgetsStatement( CurrentLine );
				break;

			// kill
			case rKILL:
				KillStatement( CurrentLine );
				break;

			// vprint
			case rVPRINT:
				VprintStatement( CurrentLine );
				break;

			// puts
			case rPUTS:
				PutsStatement( CurrentLine );
				break;

			// fputs
			case rFPUTS:
				FputsStatement( CurrentLine );
				break;

			// fgetpos
			case rFGETPOS:
				FgetposStatement( CurrentLine );
				break;

			// fsetpos
			case rFSETPOS:
				FsetposStatement( CurrentLine );
				break;

			// flush
			case rFLUSH:
				FlushStatement( CurrentLine );
				break;

			// flushall
			case rFLUSHALL:
				FlushallStatement( CurrentLine );
				break;

			// closeall
			case rCLOSEALL:
				CloseallStatement( CurrentLine );
				break;

			// lprint
			case rLPRINT:
				LprintStatement( CurrentLine );
				break;

			// unlink
			case rUNLINK:
		   	UnlinkStatement( CurrentLine );
		   	break;

			// something pretty odd going on if this ever gets called
			default:
				ReportError( GENERAL, GENERAL_ERROR, "Invalid mCommand value %03d", mCommand );
				break;
         }

		 if ( StopRequest == TRUE )
				break;
	}

	if ( IF_NESTING != 0 )
			ReportError( SYNTAX, UNBALANCED_IF, "Program ended with %d unbalanced IF/ENDIF pair%s active",
					IF_NESTING, ( IF_NESTING == 1 ) ? "" : "s" );

	if ( FOR_DEPTH != 0 )
			ReportError( SYNTAX, UNBALANCED_FOR, "Program ended with %d unbalanced FOR/ENDFOR loop%s active",
					FOR_DEPTH, ( FOR_DEPTH == 1 ) ? "" : "s" );

	if ( WHILE_DEPTH != 0 )
			ReportError( SYNTAX, UNBALANCED_WHILE, "Program ended with %d unbalanced WHILE/ENDWHILE loop%s active",
					WHILE_DEPTH, ( WHILE_DEPTH == 1 ) ? "" : "s" );

	if ( ( DEBUG == FULL ) || ( DEBUG == TERMINATE ) )
			MemoryDump();

}   // end of InterpretSourceFile

int GetCommandWord( char *Text, char *Command )
{
	if ( strlen( Text ) == strlen( Command ) )
		return ( strcmp( Text, Command ) );
	else
			return ( 1 );

}	// end of GetCommandWord

char *PullFirstWord( char *Text )
{
	static char FirstWord[ 40 ];
	int i, Length;

	Length=strcspn( Text, " ,=+-*^\t\n/" );

	for ( i=0; i<Length; i++ )
			FirstWord[ i ]=Text[ i ];

	FirstWord[ i ]='\x00';

	return ( FirstWord );

}	// end of PullFirstWord

char *PullFirstNS( char *Text )
{
	static char FirstWord[ 50 ];
	int i, Length;

	Length=strcspn( Text, ",\t\n" );

	for ( i=0; i<Length; i++ )
			FirstWord[ i ]=Text[ i ];

	FirstWord[ i ]='\x00';

	RemoveWhitespace( FirstWord );

	return ( FirstWord );

}	// end of PullFirstNS

void RemoveCommandWord( char *Text, char *Command )
{
	char *Buffer, *ptr;

	RemoveLeadingWhitespace(Text);

	Buffer=strdup(Text);

	if (Buffer == NULL)
			ReportError(GENERAL, NO_MEMORY, "Can't allocate buffer in RemoveCommandWord for Command %s", Command);

	ptr=(Buffer+strlen(Command));

	memmove(Text, ptr, strlen(ptr));
	Text[strlen(ptr)] = 0x00;
	RemoveLeadingWhitespace(Text);

	free(Buffer);

}	// end of RemoveCommandWord

void RemoveWhitespace( char *Text )
{
	RemoveLeadingWhitespace( Text );
	RemoveTrailingWhitespace( Text );

}	// end of RemoveWhitespace

void RemoveLeadingWhitespace( char *Text )
{
	char *Buffer, *ptr;
	int i=0;

	if ( !isspace( Text[ 0 ] ) )
			return;

	Buffer=strdup( Text );

	if ( Buffer == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate buffer in RemoveLeadingWhitespace for %s", Text );

	ptr=Buffer;

	while ( isspace( Buffer[ i++ ] ) ) {
		ptr++;
		if ( Buffer[ i ] == EOS )
				break;
	}

	strcpy( Text, ptr );

	free( Buffer );

}	// end of RemoveLeadingWhitespace

void RemoveTrailingWhitespace( char *Text )
{
	char *Buffer;
	int i=strlen( Text );

	Buffer=strdup( Text );
	if ( Buffer == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate buffer in RemoveTrailingWhitespace for %s", Text );

	while ( isspace( Buffer[ --i ] ) ) {
		Buffer[ i ]=EOS;
		if ( i == 0 )
				break;
	}

	strcpy( Text, Buffer );

	free( Buffer );

}	// end of RemoveTrailingWhitespace

void GetjdStatement( char *Text )
{
	ExtractParameters( "GETJD", "iIII", Text );

	iScratch1=atol( Par2 );
	iScratch2=atol( Par3 );
	iScratch3=atol( Par4 );

	sprintf( Scratch1, "%ld",
			GetJulianDay( ( int ) iScratch1, ( int ) iScratch2, ( int ) iScratch3 ) );

	SetVariable( Par1, Scratch1 );

}	// end of GetjdStatement

void JdateStatement( char *Text )
{
	int Day, Month, Year;

	ExtractParameters( "JDATE", "iiiI", Text );

	iScratch1=atol( Par4 );

	RevertJulian( iScratch1, &Day, &Month, &Year );

	sprintf( Scratch1, "%d", Day );
	SetVariable( Par1, Scratch1 );

	sprintf( Scratch1, "%d", Month );
	SetVariable( Par2, Scratch1 );

	sprintf( Scratch1, "%d", Year );
	SetVariable( Par3, Scratch1 );

}	// end of JdateStatement

long GetJulianDay( int Day, int Month, int Year )
{
	// returns the astronomer's Julian Day as a long integer

	long int JulianDay=0L;
	int jDay=0, jMonth=0, jYear=0;

	// there was no Year 0
	if ( Year == 0 )
            return 0L;

	// if BC increase by 1
	if ( Year < 0 )
            ++Year;

	// if Month=march or later, increase by 1, accept Year
	if ( Month > 2 ) {
        jYear=Year;
        jMonth=Month+1;
    }
	// otherwise decrease Year by 1 and add 1 Year+1 Month to Month
    else {
        jYear=Year-1;
		jMonth=Month+13;
    }

	// calculate julian Day number
	JulianDay = ( long ) ( floor( 365.25*jYear )+floor( 30.6001*jMonth )+Day+1720995L );

	// do we need to adjust it for the change from Julian to Gregorian calendar?

	if ( Day+31L*( Month+12L*Year ) <= START_GREGORIAN ) {
        jDay=0.01*jYear;
		JulianDay+=2-jDay+( int ) ( 0.25*jDay );
    }

	// return the long julian Day number
	return JulianDay;

}	// end of GetJulianDay

void RevertJulian( long Julian, int *Day, int *Month, int *Year )
{
	// takes a Julian Day number and returns the Day, Month, Year

	long ja, jalpha, jb, jc, jd, je;

	if ( Julian >= 2299161L ) {
		jalpha=( ( float ) ( Julian-1867216L )-0.25 )/36524.25L;
		ja=Julian+1+jalpha-( long ) ( 0.24 * jalpha );
	}
	else
			ja=Julian;

	jb=ja+1524;
	jc=6680.0+( ( float ) ( jb-2439870L )-122.1 )/365.25;
	jd=365*jc+( 0.25*jc );
	je=( jb-jd )/30.6001;
	*Day=( int ) jb-jd-( ( int ) ( 30.6001*je ) );
	*Month=( int ) je-1;

	if ( *Month > 12 )
			*Month -= 12;

	*Year=( int ) jc-4715;

	if ( *Month > 2 )
			--( *Year );

	if ( *Year <= 0 )
			--( *Year );

}	// end of RevertJulian

Label *AddLabel( char *Name, int LineNo )
{
	Label *Lptr;

	// move past the colon
	Name++;

	// no labels in the table yet
	if ( LabelStart == NULL ) {
		Lptr = ( Label * ) malloc( sizeof( Label ) );
		if ( Lptr == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Label: %s", Name );

		// fill in the data fields of the label
		if ( ( Lptr->Name = ( char * ) strdup( Name ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Label: %s", Name );

		Lptr->Line=LineNo;
		Lptr->Next=NULL;

		// set this label as the start of the Table
		LabelStart=Lptr;
	}
	else {
		// Labels exist - check we haven't already used the Label name
		if ( FindLabel( Name ) != NULL )
				ReportError( SYNTAX, DUPLICATE_LABEL, "A label with the name \"%s\" already exists", Name );

		// find the last in the list
		for ( Lptr=LabelStart; Lptr != NULL; Lptr = Lptr->Next )
				LastLabel=Lptr;

		Lptr = ( Label * ) malloc( sizeof( Label ) );
		if ( Lptr == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Label: %s", Name );

		// fill in the data fields of the label
		if ( ( Lptr->Name = ( char * ) strdup( Name ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Label: %s", Name );

		Lptr->Line=LineNo;
		Lptr->Next=NULL;

		// point the previous Label to the new Table element
		LastLabel->Next=Lptr;
	}

	return ( Lptr );

}	// end of AddLabel

Label *FindLabel( char *Name )
{
	Label *Lptr;

	for ( Lptr = LabelStart; Lptr != NULL; Lptr = Lptr->Next )
			if ( strcmp( Lptr->Name, Name ) == 0 )
					return ( Lptr );

	return ( NULL );

}	// end of FindLabel

int CheckBracketsBalance( char *LogicString )
{
	int Left=0, Right=0, i;

	// check for any brackets - if present they must be balanced
	for ( i=0; LogicString[ i ] != EOS; i++ ) {
		Right += ( LogicString[ i ] == RIGHT_PAREN ) ? 1 : 0;
		Left += ( LogicString[ i ] == LEFT_PAREN ) ? 1 : 0;
	}

	if ( Left != Right )
			return ( -1 );
	else
			return ( Right );

}	// end of CheckBracketsBalance

int GetNestingDepth( char *LogicString )
{
	int i, Depth=0, MaxDepth=0;

	if ( CheckBracketsBalance( LogicString ) < 0 )
			ReportError( SYNTAX, UNBALANCED_PAR, NULL );

	for ( i=0; LogicString[ i ] != EOS; i++ ) {
		if ( LogicString[ i ] == LEFT_PAREN )
				Depth++;
		else if ( LogicString[ i ] == RIGHT_PAREN )
				Depth--;

		MaxDepth = ( Depth > MaxDepth ) ? Depth : MaxDepth;
	}

	return ( MaxDepth );

}	// end of GetNestingDepth

int CountLeftBrackets( char *LogicElement )
{
	int i, Count=0;

	for ( i=0; LogicElement[ i ] != EOS; i++ ) {
		if ( LogicElement[ i ] == LEFT_PAREN )
				Count++;
	}

	return ( Count );

}	// end of CountLeftBrackets

int LogicResolve( char *LogicString )
{
	char Logic[ 100 ], SingleLogicExp[ 100 ];
	int Depth=0, Nesting=0, i=( -1 ), j=0, k=0, m=0, n=0;
	int Response=FALSE, TokenFetched=FALSE, CalcDone=FALSE;
	int PREVIOUS, MATH_ONLY=FALSE, LOGIC_ONLY=FALSE;
	double Answer;

	BlankString( Logic, EOS, 98 );
	BlankString( SingleLogicExp, EOS, 98 );

	Depth=GetNestingDepth( LogicString );

	for ( i=0, m=0; LogicString[ i ] != EOS; i++ ) {
		if ( ( LogicString[ i ] == LEFT_PAREN ) && ( TokenFetched == FALSE ) ) {
			Nesting++;
			if ( Nesting != Depth )
					Logic[ m++ ]=LogicString[ i ];
		}
		else {
			Logic[ m++ ]=LogicString[ i ];
			if ( LogicString[ i ] == RIGHT_PAREN )
					Nesting--;
		}

		if ( ( Nesting == Depth ) && ( TokenFetched == FALSE ) ) {
			Logic[ ++m ]  = EOS;

			for ( j=( ++i ), k=0; LogicString[ j ] != RIGHT_PAREN; i++, j++, k++ )
					SingleLogicExp[ k ] = LogicString[ j ];

			SingleLogicExp[ k ] = EOS;

			if ( strpbrk( SingleLogicExp, "+-/^*" ) == NULL )
					LOGIC_ONLY=TRUE;

			if ( strpbrk( SingleLogicExp, "!<>=" ) == NULL )
					MATH_ONLY=TRUE;

			// check whether it is a subtraction or a Unary
			if ( ( MATH_ONLY == FALSE ) && ( LOGIC_ONLY ==FALSE ) ) {

				// error if any math ops present apart from -ve operator
				if ( strpbrk( SingleLogicExp, "+/^*" ) != NULL )
						ReportError( SYNTAX, NUM_LOG, SingleLogicExp );

				// either unary or subtraction operators present
				PREVIOUS=FALSE;

				// assume we're OK until proved otherwise
				LOGIC_ONLY=TRUE;

				for ( n=0; SingleLogicExp[ n ] != EOS; n++ ) {

					if ( ( SingleLogicExp[ n ] == NEGATIVE ) && ( PREVIOUS == TRUE ) )
								ReportError( SYNTAX, BAD_UNARY, NULL );
							LOGIC_ONLY=TRUE;

					if ( isspace( SingleLogicExp[ n ] ) )
							continue;

					PREVIOUS = ( isdigit( SingleLogicExp[ n ] ) ) ? TRUE : FALSE;
				}
			}

			if ( MATH_ONLY == TRUE ) {
				strcpy( Express, SingleLogicExp );
				GetExpression( &Answer );
				sprintf( SingleLogicExp, "%f", Answer );
				strcat( Logic, SingleLogicExp );
				CalcDone=TRUE;
			}
			else if ( LOGIC_ONLY == TRUE ) {
				strcat( Logic, ( LogicEvaluate( SingleLogicExp ) == TRUE )
								? "0" : "1" );
				CalcDone=FALSE;
			}
			m=strlen( Logic );
			TokenFetched=TRUE;
		}
	}
	Logic[ m ] = EOS;

	if ( ( CountLeftBrackets( Logic ) > 1 ) || ( CalcDone == TRUE ) )
			Response=LogicResolve( Logic );
	else
			Response=ResolveLevelOne( Logic );

	return ( Response );

}	// end of LogicResolve

int ResolveLevelOne( char *LogicString )
{
	int i, j, Start=0;
	float Response, Value;
	static char Token[ 5 ]="", Operator[ 5 ]="";

	// remove the brackets if any are present
	for ( i=0; LogicString[ i ] != EOS; i++ ) {
		if ( ( LogicString[ i ] == LEFT_PAREN ) ||
				( LogicString[ i ] == RIGHT_PAREN ) ) {
					for ( j=i; LogicString[ j ] != EOS; j++ )
							LogicString[ j ] = LogicString[ j+1 ];
		}
	}

	Start = GetElement( LogicString, Token, Start );
	Value=atof( Token );

	Response = ( Value == TRUE ) ? TRUE : FALSE;

	while ( LogicString[ Start ] != EOS ) {
		Start = GetElement( LogicString, Operator, Start );
		Start = GetElement( LogicString, Token, Start );
		Value = atof( Token );

		Response = ComputeLogic( ( float ) Response, ( float ) Value, Operator );
	}

	return ( ( int ) Response );

}	// end of ResolveLevelOne

int GetElement( char *LogicString, char *Element, int Start )
{
	int i, j;

	if ( isdigit( LogicString[ Start ] ) || ( LogicString[ Start ] == NEGATIVE ) ) {
		for ( i=Start, j=0; LogicString[ i ] != EOS; i++ ) {
			if ( isspace( LogicString[ i ] ) )
					continue;

			if ( !isdigit( LogicString[ i ] )
					&& LogicString[ i ] != NEGATIVE
					&& LogicString[ i ] != POINT )
					break;

			Element[ j++ ] = LogicString[ i ];
		}
	}
	else {
		for ( i=Start, j=0; LogicString[ i ] != EOS; i++ ) {
			if ( isspace( LogicString[ i ] ) )
					continue;

			if ( isdigit( LogicString[ i ] ) )
					break;

			if ( LogicString[ i ] == NEGATIVE )
					break;

			Element[ j++ ] = LogicString[ i ];
		}
	}

	Element[ j ] = EOS;

	return ( i );

}	// end of GetElement

int LogicEvaluate( char *LogicExp )
{
	static char Lhand[ 50 ]="", Rhand[ 50 ]="", Operator[ 10 ]="";
	int i, j, Response=TRUE;
	float Left=0, Right=0;

	for ( i=0, j=0; LogicExp[ i ] == POINT ||
				   LogicExp[ i ] == NEGATIVE ||
				   isdigit( LogicExp[ i ] ) ||
				   isspace( LogicExp[ i ] ); i++ ) {
		if ( isspace( LogicExp[ i ] ) )
				continue;

		Lhand[ j++ ] = LogicExp[ i ];
	}

	Lhand[ j ]=EOS;

	Left=atof( Lhand );

	if ( i >= strlen( LogicExp ) )
			return ( Left == TRUE ) ? TRUE : FALSE;

	for ( j=0; strchr( "&|<>=!", LogicExp[ i ] ); i++ ) {
		if ( isspace( LogicExp[ i ] ) )
				continue;

		Operator[ j++ ] = LogicExp[ i ];
	}
	Operator[ j ]=EOS;

	if ( Operator[ 0 ] == EOS )
			return ( Left == TRUE ) ? TRUE : FALSE;

	for ( j=0; LogicExp[ i ] == POINT ||
			  LogicExp[ i ] == NEGATIVE ||
			  isdigit( LogicExp[ i ] ) ||
			  isspace( LogicExp[ i ] ); i++ ) {
		if ( isspace( LogicExp[ i ] ) )
				continue;

		Rhand[ j++ ] = LogicExp[ i ];
	}

	Rhand[ j ]=EOS;

	Right=atof( Rhand );

	Response=ComputeLogic( ( float ) Left, ( float ) Right, Operator );

	return ( Response );

}	// end of LogicEvaluate

int ComputeLogic( float Left, float Right, char *Operator )
{
	int Response=FALSE;

	switch ( Operator[ 0 ] ) {
	 case EQUALS : switch ( Operator[ 1 ] ) {
					     case EOS : ReportError( SYNTAX, WRONG_LOG_OP,
										"For logical EQUALS use ==" );
									break;

	                 case EQUALS  : Response = ( Left == Right ) ? TRUE : FALSE;
					 				break;

  						default   : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical EQUALS use ==" );
									break;
	               }
	               break;


	   case NOT : switch ( Operator[ 1 ] ) {
					     case EOS : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical NOT EQUALS use !=" );
									break;

	                 case EQUALS  : Response = ( Left != Right ) ? TRUE : FALSE;
					 				break;

  						default   : ReportError( SYNTAX, WRONG_LOG_OP,
										"For logical NOT EQUALS use !=" );
									break;
	               }
	               break;

	 case LESSER : switch ( Operator[ 1 ] ) {
					  case EOS : Response = ( Left < Right ) ? TRUE : FALSE;
									break;

	                 case EQUALS  : Response = ( Left <= Right ) ? TRUE : FALSE;
					 				break;

  						default   : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical LESS THAN EQUALS use <=" );
									break;
	               }
	               break;

	case GREATER : switch ( Operator[ 1 ] ) {
						case EOS : Response = ( Left > Right ) ? TRUE : FALSE;
									break;

	                 case EQUALS  : Response = ( Left >= Right ) ? TRUE : FALSE;
					 				break;

						default   : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical GREATER THAN EQUALS use >=" );
					   				break;
	               }
		     	   break;

		case AND : switch ( Operator[ 1 ] ) {
						 case EOS : ReportError( SYNTAX, INVALID_LOG_OP,
						 				"For logical AND use &&" );
									break;

	                     case AND : Response =
						 					( ( Left == TRUE ) && ( Right == TRUE ) )
											? TRUE : FALSE;
					 				break;

						default   : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical AND use &&" );
					   				break;
	               }
		     	   break;

		 case OR : switch ( Operator[ 1 ] ) {
						 case EOS : ReportError( SYNTAX, INVALID_RHS,
										"For logical OR use ||" );
									break;

	                      case OR : Response =
						  					( ( Left == TRUE ) || ( Right == TRUE ) )
											? TRUE : FALSE;
					 				break;

						default   : ReportError( SYNTAX, INVALID_LOG_OP,
										"For logical OR use ||" );
					   				break;
	               }
				   break;

  	    default  : ReportError( SYNTAX, INVALID_LOG_OP, Operator );
				   break;
	}

	return( Response );

}	// end of ComputeLogic

void AbsStatement( char *Text )
{
	ExtractParameters( "ABS", "iI", Text );

	sprintf( Scratch1, "%ld", atol( Par2 ) );

	SetVariable( Par1, Scratch1 );

}	// end of AbsStatement

void FabsStatement( char *Text )
{
	ExtractParameters( "FABS", "fF", Text );

	sprintf( Scratch1, "%f", atof( Par2 ) );

	SetVariable( Par1, Scratch1 );

}	// end of FabsStatement

void RandStatement( char *Text )
{
	ExtractParameters( "RAND", "iI", Text );

	sprintf( Scratch1, "%ld", (long) ( rand() % atoi( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of RandStatement

void FrandStatement( char *Text )
{
	ExtractParameters( "FRAND", "f", Text );

	iScratch1 = rand() % 1000;
	iFloat1 = ( float ) ( iScratch1 / 1000.0 );

	sprintf( Scratch1, "%.4f", iFloat1 );

	SetVariable( Par1, Scratch1 );

}	// end of RandStatement

void SinStatement( char *Text )
{
	ExtractParameters( "SIN", "fF", Text );

	sprintf( Scratch1, "%f", sin( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of SinStatement

void CosStatement( char *Text )
{
	ExtractParameters( "COS", "fF", Text );

	sprintf( Scratch1, "%f", cos( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of CosStatement

void TanStatement( char *Text )
{
	ExtractParameters( "TAN", "fF", Text );

	sprintf( Scratch1, "%f", tan( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of TanStatement

void AsinStatement( char *Text )
{
	ExtractParameters( "ASIN", "fF", Text );

	sprintf( Scratch1, "%f", asin( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of AsinStatement

void AcosStatement( char *Text )
{
	ExtractParameters( "ACOS", "fF", Text );

	sprintf( Scratch1, "%f", acos( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of AcosStatement

void AtanStatement( char *Text )
{
	ExtractParameters( "ATAN", "fF", Text );

	sprintf( Scratch1, "%f", atan( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of AtanStatement

void RadStatement( char *Text )
{
	ExtractParameters( "RAD", "fF", Text );

	sprintf( Scratch1, "%f", atof( Par2 ) / RADIAN );

	SetVariable( Par1, Scratch1 );

}	// end of RadianStatement

void DegStatement( char *Text )
{
	ExtractParameters( "DEG", "fF", Text );

	sprintf( Scratch1, "%f", RADIAN * atof( Par2 ) );

	SetVariable( Par1, Scratch1 );

}	// end of DegStatement

void ExpStatement( char *Text )
{
	ExtractParameters( "EXP", "fF", Text );

	sprintf( Scratch1, "%f", exp( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of ExpStatement

void SqrStatement( char *Text )
{
	ExtractParameters( "SQR", "fF", Text );

	iFloat1=atof( Par2 );

	sprintf( Scratch1, "%f", ( iFloat1 * iFloat1 ) );

	SetVariable( Par1, Scratch1 );

}	// end of SqrStatement

void LnStatement( char *Text )
{
	ExtractParameters( "LN", "fF", Text );

	sprintf( Scratch1, "%f", log( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of LnStatement

void LogStatement( char *Text )
{
	ExtractParameters( "LOG", "fF", Text );

	sprintf( Scratch1, "%f", log10( atof( Par2 ) ) );

	SetVariable( Par1, Scratch1 );

}	// end of LogStatement

int ExtractParameters( char *Function, char *Format, char *Text )
{
	double Answer;
	int i, j, QuoteBalanced, Check;
	char *CurrentPoint, Buffer[ 250 ];
	vList *vPtr;

	RemoveCommandWord( Text, Function );

	if ( ( strlen( Text ) == 0 ) && ( *Format != DOLLAR ) )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, ExpandSyntax( Format ) );

	CurrentPoint = Text;

	for ( i = 0; Format[ i ] != EOS; i++ ) {

		// check for over-run of the array
		if ( i > MAX_PARS )
				break;

		// if we are already at the end of the parameter text string, but there are indicators in the format string left to process, flag an error
		if ( ( *CurrentPoint == EOS ) && ( Format[ i ] != DOLLAR ) )
			ReportError( SYNTAX, GENERIC_MISS_PAR, "%s statement missing the %d%s parameter\n> %s %s", Function, i+1, Suffix( i+1 ), Function, ExpandSyntax( Format ) );

		if ( ( *CurrentPoint == EOS ) && ( Format[ i ] == DOLLAR ) ) {
			strcpy( Variables[ i ], "" );
			break;
		}

		if ( Format[ i ] == DOLLAR ) {
			strcpy( Variables[ i ], CurrentPoint );
			break;
		}

		switch ( Format[ i ] ) {

			case  'i':
			case  'I':
			case  'f':
			case  'F':  // get an integer or a float
						j=0;

						Check=toupper( Format[ i ] );


				if ( *CurrentPoint == QUOTE )
					ReportError( SYNTAX, MISPLACED_STR, "%d%s parameter of the %s statement should be a%s %s\n> %s %s", i+1, Suffix( i+1 ), Function,
							( Check == 'I') ? "n Integer" : " Float", ( islower( Format[ i ] ) ) ? "Variable" : "Value", Function, ExpandSyntax( Format ) );

						// gather the parameter into the buffer
						while ( ( *CurrentPoint != EOS ) && ( *CurrentPoint != ',') )
								Buffer[ j++ ]=*CurrentPoint++;

						// cap the buffer
						Buffer[ j ] = EOS;

				if ( ( Lastch( Buffer ) == DOLLAR ) || ( ( Check == 'F') && ( Lastch( Buffer ) == PERCENT ) ) || ( ( Format[ i ] == 'i') && ( Lastch( Buffer ) != PERCENT ) ) )
					ReportError( SYNTAX, MISPLACED_STR, "%d%s parameter of the %s statement should be a%s %s\n> %s %s", i+1, Suffix( i+1 ), Function,
							( Check == 'I') ? "n Integer" : " Float", ( islower( Format[ i ] ) ) ? "Variable" : "Value", Function, ExpandSyntax( Format ) );

						// take a copy of the buffer
						strcpy( Variables[ i ], Buffer );

						if ( islower( Format[ i ] ) ) {
							// MUST be a valid variable name not an expression
							if ( strpbrk( Variables[ i ], "+-/^*" ) != NULL )

					ReportError( SYNTAX, MISPLACED_EXP, "%d%s parameter of the %s statement should be a%s Variable\n> %s %s",
							i+1, Suffix( i+1 ), Function, ( Check == 'I') ? "n Integer" : " Float", Function, ExpandSyntax( Format ) );

							vPtr=LookUp( Buffer, ALL );

							if ( vPtr != NULL )
									strcpy( Buffer, vPtr->vValue );
							else
									ReportError( SYNTAX, UNKNOWN_VARIABLE, Buffer );
						}
						else {

							ReplaceVariables( Variables[ i ], CALCULATE );

							if ( strpbrk( Variables[ i ], "+-/^*" ) != NULL ) {
									strcpy( Express, Variables[ i ] );
									GetExpression( &Answer );
									if ( Format[ i ] == 'F')
											sprintf( Variables[ i ], "%f", Answer );
									else
											sprintf( Variables[ i ], "%d", ( int ) Answer );
							}
						}

						break;

			case  's':
			case  'S':  // get a string
						QuoteBalanced=j=0;

						// this sequence is for variables, not literal strings

						if ( *CurrentPoint != QUOTE ) {
							while ( ( *CurrentPoint != EOS ) && ( *CurrentPoint != ',') ) {
								Buffer[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Buffer[ j ] = EOS;

				if ( Lastch( Buffer ) != DOLLAR )
					ReportError( SYNTAX, NOT_A_STR_VAR, "%d%s parameter of the %s statement should be a string %s\n> %s %s", i+1, Suffix( i+1 ), Function,
						( islower( Format[ i ] ) ) ? "Variable" : "Value", Function, ExpandSyntax( Format ) );

							// MUST be a valid variable name
							vPtr=LookUp( Buffer, ALL );

							if ( vPtr != NULL ) {
								// if we don't need the variable name passed back then pass back the value

								if ( Format[ i ] == 'S')
										strcpy( Buffer, vPtr->vValue );
							}
							else
									ReportError( SYNTAX, UNKNOWN_VARIABLE, Buffer );

						}
						// this caters for literal strings
						else {
							if ( Format[ i ] == 's') {
								// MUST be a valid variable name, but this is a literal value

								ReportError( SYNTAX, BAD_LITERAL, "%s %s", Function, ExpandSyntax( Format ) );
							}

							// move past the first QUOTE
							CurrentPoint++;
							while ( ( *CurrentPoint != EOS ) && ( QuoteBalanced != 1 ) ) {
								if ( ( *CurrentPoint == QUOTE ) && ( Buffer[ j-1 ] != FSLASH ) ) {
									QuoteBalanced = 1;
									CurrentPoint++;
								}
								else
										Buffer[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Buffer[ j ] = EOS;
						}

						// take a copy of the buffer
						strcpy( Variables[ i ], Buffer );

						break;

			case  'x':  // get a filename
						QuoteBalanced=j=0;

						/*
							* this sequence is for variables, not literal strings
							*
							*/
						if ( *CurrentPoint != QUOTE ) {
							while ( ( *CurrentPoint != EOS ) && ( *CurrentPoint != ',') ) {
								Buffer[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Buffer[ j ] = EOS;

				if ( Lastch( Buffer ) != DOLLAR )
					ReportError( SYNTAX, INVALID_FILENAME, "%d%s parameter of the %s statement should either be a filename \n> in quotes or a string variable: \"FileName\" or FileName$"
							"\n> %s %s", i+1, Suffix( i+1 ), Function, Function, ExpandSyntax( Format ) );

							// MUST be a valid variable name
							vPtr=LookUp( Buffer, ALL );

							if ( vPtr != NULL ) {
									strcpy( Buffer, vPtr->vValue );
							}
							else
									ReportError( SYNTAX, UNKNOWN_VARIABLE,
											Buffer );

						}
						// this caters for literal strings
						else {
							// move past the first QUOTE
							CurrentPoint++;
							while ( ( *CurrentPoint != EOS ) && ( QuoteBalanced != 1 ) ) {
								if ( ( *CurrentPoint == QUOTE ) && ( Buffer[ j ] != FSLASH ) ) {
									QuoteBalanced = 1;
									CurrentPoint++;
								}
								else
									Buffer[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Buffer[ j ] = EOS;
						}

						// take a copy of the buffer
						strcpy( Variables[ i ], Buffer );

						break;


			  default:  // anything else is an error
						strcpy( Variables[ i ], "Error!" );
						break;
		 }

		 // move past any commas or spaces
		 while ( ( *CurrentPoint != EOS ) && ( ( *CurrentPoint == ' ')
				|| ( *CurrentPoint == ',') ) )

				CurrentPoint++;
	}

	return ( 0 );

}	// end of ExtractParameters

char *ExpandSyntax( char *Format )
{
	int i, Last;
	static char Parsed[ 80 ];

	Last = strlen( Format );

	sprintf( Parsed, "%s", "" );

	for ( i=0; i<Last; i++ ) {

		switch ( Format[ i ] ) {

			case  'i': 	strcat( Parsed, "IntVar%" );
						break;

			case  'I': 	strcat( Parsed, "Int%" );
						break;

			case  'f': 	strcat( Parsed, "FloatVar" );
						break;

			case  'F': 	strcat( Parsed, "Float" );
						break;

			case  's': 	strcat( Parsed, "StringVar$" );
						break;

			case  'S': 	strcat( Parsed, "String$" );
						break;

			case  '$': 	strcat( Parsed, "[ OptionalPars ... ]" );
						break;

			case  'x': 	strcat( Parsed, "\"FileName\"" );
						break;

			  default:  strcat( Parsed, "?Unknown?" );
						break;
		 }

		 if ( ( Last > 1 ) && ( i != ( Last-1 ) ) )
				strcat( Parsed, ", " );
	}

	return ( Parsed );

}	// end of ExpandSyntax

int PrintStatement( char *Text )
{
	ExtractParameters( "PRINT", "S$", Text );

	return ( printf( ExtractPrintParameters( Par1, Par2 ) ) );

}	// end of PrintStatement

int FprintStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FPRINT", "xS$", Text );

	fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN, "Define the file with FILE, then open it using OPEN" );

	return ( fprintf( fPtr->Handle, ExtractPrintParameters( Par2, Par3 ) ) );

}	// end of FprintStatement

void VprintStatement( char *Text )
{
	ExtractParameters( "VPRINT", "sS$", Text );

	SetVariable( Par1, ExtractPrintParameters( Par2, Par3 ) );

}	// end of VprintStatement

void PutsStatement( char *Text )
{
	ExtractParameters( "PUTS", "S", Text );

	ScanForSlashOpts( Par1 );
	puts( Par1 );

}	// end of PutsStatement

void FputsStatement( char *Text )
{
	File *fPtr;

	ExtractParameters( "FPUTS", "xS", Text );

    fPtr=fLookUp( Par1 );

	if ( fPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_FILE, "Unknown filename: %s", Par1 );

	if ( fPtr->Open == FALSE )
			ReportError( SYNTAX, FILE_NOT_OPEN,
			"Define the file with FILE, then open it using OPEN" );

	ScanForSlashOpts( Par2 );
	fputs( Par2, fPtr->Handle );

}	// end of FputsStatement

char *ExtractPrintParameters( char *Format, char *Text )
{
	static char Buffer[ 250 ]="";
	char *CurrentPoint, Next, DigBuff[ 10 ], Temp[ 100 ]="", Pulled[ 100 ]="";
	double Answer, Digits, P, W;
	int i, j, QuoteBalanced, Prec, Width, NoPars=FALSE, WidthSpecifiers=FALSE;

	CurrentPoint=Text;
	BlankString( Buffer, EOS, 248 );

	// check for format string but no detail string
	// check for accepting strings with \" in them
	// cater for leading zeros in format specifiers

	if ( *Text == 0x00 )
			NoPars=TRUE;

	ScanForSlashOpts( Format );

	for ( i=0; Format[ i ] != EOS; i++ ) {

		if ( ( Format[ i ] != PERCENT ) || ( NoPars == TRUE ) ) {
			sprintf( Temp, "%c", Format[ i ] );
			strcat( Buffer, Temp );
			if ( Format[ i ] == PERCENT )
					strcat( Buffer, Temp );
		}
		else {

			// format specifiers but no parameters left in the Text string?
			if ( CurrentPoint >= ( strlen( Text )+Text ) ) {
				NoPars = TRUE;
				break;
			}

			strcpy( DigBuff, "0.0" );

			// check whether the next character is a digit, - or + and gather digits if it is
			Next=Format[ i+1 ];

			if ( Next == EOS ) {
				strcat( Buffer, "%" );
				break;
			}

			j=0;

			if ( ( Next == NEGATIVE ) || ( Next == POSITIVE ) || ( Next == POINT ) || ( isdigit( Next ) ) ) {
				DigBuff[ j++ ]=Next;
				i++;
				Next=Format[ i+1 ];
			}

			DigBuff[ j ]=EOS;

			Digits=atof( DigBuff );
			Prec=Width=0;

			if ( Digits != 0.0 ) {
				P=( float ) modf( Digits, &W );
				Width= ( int ) W;
				Prec = ( int ) ( P*10 );
			}

			switch ( Next ) {
				// for each type, acquire from the Text string the next literal value,
				// parameter or expression and set up a format for it to be printed into

			   case 'r':
			   case 'R':
			   case 'F':
			   case 'f': // gather the parameter into the buffer
						 j=0;
						 while ( ( *CurrentPoint != EOS ) && ( *CurrentPoint != ',') )
								Pulled[ j++ ]=*CurrentPoint++;

						 // cap the buffer
						 Pulled[ j ] = EOS;

						 ReplaceVariables( Pulled, PRINT );

						 if ( strpbrk( Pulled, "+-/^*" ) != NULL ) {
						 		strcpy( Express, Pulled );
								GetExpression( &Answer );
						 }
						 else
								Answer=atof( Pulled );

						 // WidthSpecifiers=TRUE;

						 if ( ( Digits == 0.0 ) && ( WidthSpecifiers == FALSE ) ) {
							Width=10;
							Prec =6;
						}

						 if ( Width < 0 )
								sprintf( Temp, "%-*.*f", abs( Width ), Prec, Answer );
						 else
						 		sprintf( Temp, "%*.*f", Width, Prec, Answer );

						 strcat( Buffer, Temp );
						 i++;
						 break;

			  case  'i':
			  case  'I':
			  case  'D':
			  case  'd': // gather the parameter into the buffer
						 j=0;
						 while ( ( *CurrentPoint != EOS ) &&
										( *CurrentPoint != ',') )
								Pulled[ j++ ]=*CurrentPoint++;

						 // cap the buffer
						 Pulled[ j ] = EOS;

						 ReplaceVariables( Pulled, PRINT );

						 if ( strpbrk( Pulled, "+-/^*" ) != NULL ) {
						 		strcpy( Express, Pulled );
								GetExpression( &Answer );
						 }
						 else
								Answer=atol( Pulled );

						 if ( Width < 0 )
								sprintf( Temp, "%-*ld", abs( Width ),
										( long ) Answer );
						 else
								sprintf( Temp, "%*ld", Width, ( long ) Answer );

						 strcat( Buffer, Temp );
						 i++;
						 break;

			  case  'S':
			  case  's': // gather the parameter into the buffer
						 QuoteBalanced=j=0;

						 if ( *CurrentPoint != QUOTE ) {
							while ( ( *CurrentPoint != EOS ) && ( *CurrentPoint != ',') ) {
								Pulled[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Pulled[ j ] = EOS;
							ReplaceVariables( Pulled, PRINT );
						 }
						 else {
							// move past the first QUOTE
							CurrentPoint++;
							while ( ( *CurrentPoint != EOS ) && ( QuoteBalanced != 1 ) ) {
								if ( ( *CurrentPoint == QUOTE ) && ( Pulled[ j-1 ] != FSLASH ) ) {
									QuoteBalanced = 1;
									CurrentPoint++;
								}
								else
									Pulled[ j++ ]=*CurrentPoint++;
							}
							// cap the buffer
							Pulled[ j ] = EOS;
						 }

						 ScanForSlashOpts( Pulled );

						 if ( Width < 0 )
								sprintf( Temp, "%-*s", abs( Width ), Pulled );
						 else
						 		sprintf( Temp, "%*s", Width, Pulled );

						 strcat( Buffer, Temp );
						 i++;
						 break;

		  		default: strcat( Buffer, "%%" );
						 break;
			}

			// move past any comma and any spaces
			while ( ( *CurrentPoint != EOS ) && ( ( *CurrentPoint == ' ') || ( *CurrentPoint == ',') ) )
					CurrentPoint++;
		}
	}

	return ( Buffer );

}	// end of ExtractPrintParameters

void ScanForSlashOpts( char *Text )
{
	int i;
	char Temp[ 10 ], Buffer[ 100 ]="", Next;

	for ( i=0; Text[ i ] != EOS; i++ ) {

		if ( Text[ i ] == FSLASH ) {
			Next=Text[ i+1 ];

			if ( Next == EOS ) {
				strcat( Buffer, "\\" );
				break;
			}
			switch ( Next ) {
				case  'n':  strcat( Buffer, "\n" );
							i++;
							break;

				case  'r':  strcat( Buffer, "\r" );
							i++;
							break;

				case  't':  strcat( Buffer, "\t" );
							i++;
							break;

				case  'a':  strcat( Buffer, "\a" );
							i++;
							break;

				case  'f':  strcat( Buffer, "\f" );
							i++;
							break;

				case  'v':  strcat( Buffer, "\v" );
							i++;
							break;

				case  'b':  strcat( Buffer, "\b" );
							i++;
							break;

			 case  FSLASH:  strcat( Buffer, "\\" );
							i++;
							break;

			 case  SQUOTE:  strcat( Buffer, "\'" );
							i++;
							break;

			  case  QUOTE:  strcat( Buffer, "\"" );
							i++;
							break;

				  default:  break;
			}
		}
		else {
			sprintf( Temp, "%c", Text[ i ] );
			strcat( Buffer, Temp );
		}
	}

	strcpy( Text, Buffer );

}	// end of ScanForSlashOpts

int LprintStatement( char *Text )
{
	/* int Status;

	ExtractParameters( "LPRINT", "S$", Text );

	Status=biosprint( 1, 0, 0 );

    if ( ( Status & 0x80 ) != 0 )
			ReportError( GENERAL, PRN_BUSY_OFF, NULL );

    if ( ( Status & 0x20 ) != 0 )
			ReportError( GENERAL, PRN_DISCONNECT, NULL );

	// return ( fprintf( stdprn, ExtractPrintParameters( Par1, Par2 ) ) );*/

	return ( 0 );

}   // end of LprintStatement

int ProcStatement( char *Text, int Start )
{
	Proc *ProcPtr;

	if ( PROC_CALLS >= ( MAX_PROC_DEPTH-1 ) )
			ReportError( GENERAL, PROC_TOO_DEEP, "Maximum depth allowed for nested procedures is %04d", MAX_PROC_DEPTH );

	RemoveCommandWord( Text, "PROC" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_PROC, NULL );

	if ( Start == 1 ) {
		if ( strcmp( PullFirstWord( Text ), "Main" ) == 0 )
				ReportError( SYNTAX, CALLING_MAIN,
					"The Main procedure is called once automatically by the interpreter\n> it cannot be called directly from within programs" );
	}

	ProcPtr=FindProc( PullFirstWord( Text ) );

	if ( ProcPtr == NULL )
			ReportError( SYNTAX, UNKNOWN_PROC, NULL );

	// remove the name of the procedure from the text string
	RemoveCommandWord( Text, PullFirstWord( Text ) );

	// check for passed variables
	PassVariables( Text, ProcPtr->Line, PROC_CALLS+1 );

	// record the line to go back to upon return or endproc
	RETURN_NEST[ ++PROC_RETURNS ]=CURRENT_LINE;

	// record the depth of if nesting on the entry to this procedure
	PROC_DEPTH[ PROC_CALLS++ ]=IF_NESTING;

	// record whether this is the deepest level of proc nesting so far
	DeepestProc = ( PROC_CALLS > DEEPEST_PROC ) ? ProcPtr->Name : DeepestProc;
	DEEPEST_PROC = ( PROC_CALLS > DEEPEST_PROC ) ? PROC_CALLS : DEEPEST_PROC;

	// keep the name of the current procedure in a local variable that can be referenced
	// by the interpreter if required - might be removed once the debugging is over

	FixVariable( "_ProcName$", ProcPtr->Name, PROC_CALLS, NULL );

	return ( ProcPtr->Line );

}	// end of ProcStatement

int EndprocStatement( void )
{
	int Line;

	--PROC_CALLS;

	if ( PROC_CALLS < 0 )
			ReportError( GENERAL, BAD_ENDPROC, "PROC_CALLS=%d", PROC_CALLS );

	if ( PROC_CALLS != 0 )
			KillVariables();

	Line=RETURN_NEST[ PROC_RETURNS-- ];
	IF_NESTING=PROC_DEPTH[ PROC_CALLS ];

	return ( Line );

}	// end of EndprocStatement

Proc *AddProc( char *Name, int LineNo )
{
	Proc *ProcPtr;
	char *NamePtr;

	RemoveCommandWord( Name, "DEFPROC" );

	if ( strlen( Name ) == 0 )
			ReportError( SYNTAX, NO_PROC_NAME, NULL );
	else
			NamePtr=PullFirstWord( Name );

	// no Procs in the table yet
	if ( ProcStart == NULL ) {

		ProcPtr = ( Proc * ) malloc( sizeof( Proc ) );

		if ( ProcPtr == NULL )
				ReportError( GENERAL, NO_MEMORY,	"Can't allocate memory for Proc: %s", NamePtr );

		// fill in the data fields of the Proc
		if ( ( ProcPtr->Name = ( char * ) strdup( NamePtr ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Proc: %s", NamePtr );

		ProcPtr->Line=LineNo;
		ProcPtr->Next=NULL;

		// set this Proc as the start of the Table
		ProcStart=ProcPtr;
	}
	else {

		// Procs exist - check we haven't already used the Proc name
		if ( FindProc( NamePtr ) != NULL )
				ReportError( GENERAL, DUPLICATE_PROC, "A Proc with the name \"%s\" already exists", NamePtr );

		// find the last in the list
		for ( ProcPtr=ProcStart; ProcPtr != NULL; ProcPtr = ProcPtr->Next )
				LastProc=ProcPtr;

		ProcPtr = ( Proc * ) malloc( sizeof( Proc ) );

		if ( ProcPtr == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Proc: %s", NamePtr );

		// fill in the data fields of the Proc
		if ( ( ProcPtr->Name = ( char * ) strdup( NamePtr ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Proc: %s", NamePtr );

		ProcPtr->Line=LineNo;
		ProcPtr->Next=NULL;

		// point the previous Proc to the new Table element
		LastProc->Next=ProcPtr;
	}

	return ( ProcPtr );

}	// end of AddProc

Proc *FindProc( char *Name )
{
	struct Proc *ProcPtr;

	for ( ProcPtr = ProcStart; ProcPtr != NULL; ProcPtr = ProcPtr->Next )
			if ( strcmp( ProcPtr->Name, Name ) == 0 )
					return ( ProcPtr );

	return ( NULL );

}	// end of FindProc

void PassVariables( char *Text, int pLine, int Level )
{
	char *VarName, *PassName, *TempName, *DefLine;
	int Passed=FALSE, NonePassed=FALSE, NoneReqd=FALSE;
	int pType, rType, pMore, rMore;
	struct vList *vPtr;

	if ( ( PassName=( char * ) malloc( 50 ) ) == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for PassName buffer" );

	if ( ( VarName=( char * ) malloc( 50 ) ) == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for VarName buffer" );

	if ( ( TempName=( char * ) malloc( 50 ) ) == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for TempName buffer" );

	// copy the DEFPROC line
	DefLine=strdup( Source[ pLine ] );
	if ( DefLine == NULL )
			ReportError( GENERAL, NO_MEMORY,
					"Can't allocate memory for DefLine buffer" );

	RemoveCommandWord( DefLine, "DEFPROC" );
	RemoveCommandWord( DefLine, PullFirstWord( DefLine ) );

	if ( strlen( DefLine ) == 0 )
			NoneReqd=TRUE;

	if ( strlen( Text ) == 0 )
			NonePassed=TRUE;

	if ( ( NoneReqd == TRUE ) && ( NonePassed == TRUE ) ) {
		free( DefLine );
		free( PassName );
		free( VarName );
		free( TempName );

		return;
	}

	if ( ( NoneReqd == TRUE ) && ( NonePassed == FALSE ) )
			ReportError( SYNTAX, NOT_TAKE_PAR, NULL );

	if ( ( NoneReqd == FALSE ) && ( NonePassed == TRUE ) )
			ReportError( SYNTAX, NO_PARS_PASSED, NULL );

	while ( strlen( DefLine ) != 0 ) {

		Passed=FALSE;

		while ( isspace( Text[ 0 ] ) || ( ispunct( Text[ 0 ] ) ) ) {
			if ( Text[ 0 ] == HASH )
					Passed=TRUE;

			if ( Text[ 0 ] == QUOTE )
					break;

			if ( Text[ 0 ] == EOS )
					return;
			else
					Text++;
		}

		strcpy( PassName, PullFirstWord( DefLine ) );
		strcpy( VarName, PullFirstNS( Text ) );
		strcpy( TempName, VarName );

		// make an assumption and see if we need to change it
		pType=pINTEGER;

		if ( VarName[ 0 ] == QUOTE ) {
			pType=pDATA;
			if ( Lastch( PassName ) != '$')
					ReportError( SYNTAX, MISMATCHED_PARS, Source[ pLine ] );
		}
		else if ( strpbrk( VarName, "+-*^/" ) != NULL ) {
			if ( Lastch( PassName ) == '$')
					ReportError( SYNTAX, MISMATCHED_PARS, Source[ pLine ] );
			pType=pDATA;
		}
		else if ( isdigit( VarName[ 0 ] ) ) {
			if ( Passed == TRUE )
					ReportError( SYNTAX, BAD_PASS_NUMERIC, NULL );

			if ( Lastch( PassName ) == '$')
					ReportError( SYNTAX, MISMATCHED_PARS, Source[ pLine ] );

			pType=pDATA;
		}

		if ( pType != pDATA ) {
			// get the data type of the passed parameter
			switch ( Lastch( VarName ) ) {
				case  '%' : pType=pINTEGER;
							break;

				case  '$' : pType=pSTRING;
							break;

				  default : pType=pFLOAT;
							break;
			}

			// get data type of the receiving parameter
			switch ( Lastch( PassName ) ) {
				case  '%' : rType=pINTEGER;
							break;

				case  '$' : rType=pSTRING;
							break;

				  default : rType=pFLOAT;
							break;
			}
		}

		// are we passing incorrect values - allow silent casting of integer
		// into float and vice versa, but not from numerical into string

		if ( pType != pDATA ) {
			if ( ( ( rType == pSTRING ) && ( pType != pSTRING ) )
					|| ( ( pType == pSTRING ) && ( rType != pSTRING ) ) )
					ReportError( SYNTAX, MISMATCHED_PARS, Source[ pLine ] );
		}

		if ( pType != pDATA ) {
			// if we are not being passed literal
			// data, do a variable lookup

			vPtr=LookUp( VarName, ALL );

			// couldn't find variable, report error
			if ( vPtr == NULL )
					ReportError( SYNTAX, UNKNOWN_PASSEE, "\"%s\"", VarName );
			else {
				// check that they haven't tried to pass a global variable or constant by reference
				if ( ( vPtr->Constant == TRUE ) && ( Passed == TRUE ) )
						ReportError( SYNTAX, BAD_PASS_CONSTANT, NULL );

				if ( ( vPtr->Offset == GLOBAL ) && ( Passed == TRUE ) )
						ReportError( SYNTAX, BAD_PASS_GLOBAL, NULL );

				// assign a new local variable with the passed value
				FixVariable( PassName, vPtr->vValue, Level,
						( Passed == TRUE ) ? VarName : NULL );
			}
		}
		else
				// assign a new local variable
				FixVariable( PassName, VarName, Level, NULL );

		// remove this variable from the list of passed variables
		RemoveCommandWord( Text, TempName );

		// remove this variable from the list of receptors
		RemoveCommandWord( DefLine, PassName );

		rMore=strlen( DefLine );
		pMore=strlen( Text );

		// raise error if too few parameters passed
		if ( ( pMore == 0 ) && ( rMore > 0 ) )
				ReportError( SYNTAX, TOO_FEW_PARS, NULL );

		// raise error if too many parameters passed
		if ( ( rMore == 0 ) && ( pMore > 0 ) )
				ReportError( SYNTAX, TOO_MANY_PARS, NULL );
	}

	// free up all the allocated memory again
	free( DefLine );
	free( PassName );
	free( VarName );
	free( TempName );

}	// end of PassVariables

void GetExpression( double *Result )
{
	ExpressPtr=Express;

	GetToken();

    if ( !*Token )
         	ReportError( SYNTAX, BAD_TOKEN, NULL );

    AddOrSub( Result );

}	// end of GetExpression

void AddOrSub( double *Result )
{
    char op;
    double hold;

    MultOrDiv( Result );

    while ( ( op = *Token ) == '+' || op == '-') {
        GetToken();
        MultOrDiv( &hold );

        ResolveArithmetic( op, Result, &hold );
    }

}	// end of AddOrSub

void MultOrDiv( double *Result )
{
    char op;
    double hold;

    Exponentiate( Result );

    while ( ( op = *Token ) == '*' || op == '/' || op == '%') {
        GetToken();
        Exponentiate( &hold );

        ResolveArithmetic( op, Result, &hold );
    }

}	// end of MultOrDiv

void Exponentiate( double *Result )
{
    double hold;

    ResolveOrZap( Result );

    if ( *Token == '^') {
        GetToken();
        Exponentiate( &hold );

        ResolveArithmetic('^', Result, &hold );
    }

}	// end of Exponentiate

void ResolveOrZap( double *Result )
{
    char op;

    op = 0;

    if ( ( TokenType == DELIMITER ) && ( *Token == '+' || *Token == '-') ) {
        op = *Token;
        GetToken();
    }

    CheckBrackets( Result );

    if ( op )
            Unary( op, Result );

}	// end of ResolveOrZap

void CheckBrackets( double *Result )
{
    if ( *Token == '(' && ( TokenType == DELIMITER ) ) {
        GetToken();
        AddOrSub( Result );

        if ( *Token != ')')
				ReportError( SYNTAX, UNBALANCED_PAR, NULL );

        GetToken();
    }
    else
            Primitive( Result );

}	// end of CheckBrackets

void Primitive( double *Result )
{
    if ( TokenType == NUMBER ) {
        *Result=atof( Token );
        GetToken();
        return;
    }

	ReportError( SYNTAX, BAD_PRIMITIVE, "Invalid primitive ( non-numeric ) "
			"found in expression" );

}	// end of Primitive

void ResolveArithmetic( char operator, double *R, double *H )
{
    float t, ex;

    switch( operator ) {
        case '-': *R=*R - *H;
                  break;

        case '+': *R=*R + *H;
                  break;

        case '*': *R=( *R ) * ( *H );
                  break;

        case '/': if ( 0 == ( int ) *H )
						ReportError( GENERAL, DIV_BY_ZERO, NULL );
				  else
				  		*R=( *R ) / ( *H );
                  break;

        case '^': ex = *R;
                  if ( *H == 0 ) {
                       *R = 1;
                       break;
                  }
                  for ( t=*H-1; t>0; --t )
                  		*R = ( *R ) * ex;

                  break;
    }

}	// end of ResolveArithmetic

void Unary( char operator, double *R )
{
    if ( operator == '-')
            *R = -( *R );

}	// end of Unary

void GetToken( void )
{
    char *Temp;

    TokenType=0;
    Temp=Token;

    while ( isspace( *ExpressPtr ) )
            ++ExpressPtr;

    if ( strchr( "+-*/%^=()", *ExpressPtr ) ) {
        TokenType=DELIMITER;
        *Temp++ = *ExpressPtr++;
    }
    else if ( isdigit( *ExpressPtr ) ) {
        while ( !IsDelim( *ExpressPtr ) )
            *Temp++ = *ExpressPtr++;

        TokenType=NUMBER;
    }

    *Temp='\0';

}	// end of GetToken

int IsDelim( char c )
{
    if ( strchr( " +-*/^=()",  c ) || c == 9 || c == '\r' || c == 0 )
        	return ( 1 );
    else
    		return ( 0 );

}	// end of IsDelim

void StrlenStatement( char *Text )
{
	ExtractParameters( "STRLEN", "iS", Text );

	sprintf( Scratch1, "%d", ( int ) strlen( Par2 ) );

	SetVariable( Par1, Scratch1 );

}	// end of StrlenStatement

void LeftStrStatement( char *Text )
{
	ExtractParameters( "LEFTSTR", "sSI", Text );

	iScratch1=labs( atol( Par3 ) );

	if ( iScratch1 < ( long ) strlen( Par2 ) )
			Par2[ ( int ) iScratch1 ]=EOS;

	SetVariable( Par1, Par2 );

}	// end of LeftStrStatement

void RightStrStatement( char *Text )
{
	ExtractParameters( "RIGHTSTR", "sSI", Text );

	iScratch1=labs( atol( Par3 ) )-1;

	iScratch1 = ( iScratch1 < 0L ) ? 0L : iScratch1;

	if ( iScratch1 < ( long ) strlen( Par2 ) )
			dScratch1=Par2+ ( int ) iScratch1;
	else
			dScratch1=Par2;

	SetVariable( Par1, dScratch1 );

}	// end of RightStrStatement

void LowerStrStatement( char *Text )
{
	ExtractParameters( "LOWERSTR", "sS", Text );

	SetToLowercase( Par2 );

	SetVariable( Par1, Par2 );

}	// end of LowerStrStatement

void UpperStrStatement( char *Text )
{
	ExtractParameters( "UPPERSTR", "sS", Text );

	SetToUppercase( Par2 );

	SetVariable( Par1, Par2 );

}	// end of UpperStrStatement

void CopyStrStatement( char *Text )
{
	ExtractParameters( "COPYSTR", "sS", Text );

	SetVariable( Par1, Par2 );

}	// end of CopyStrStatement

void StrCatStatement( char *Text )
{
	vList *vPtr;

	ExtractParameters( "STRCAT", "sS", Text );

	vPtr=LookUp( Par1, PROC_CALLS );

	// buffer space big enough for vPtr->vValue and Par2 + fudge space
	dScratch1=( char * ) malloc( ( strlen( vPtr->vValue )+strlen( Par2 ) )+10 );

	if ( dScratch1 == NULL )
			ReportError( GENERAL, NO_MEMORY, "STRCAT: Can't allocate memory for char *dScratch1" );

	strcpy( dScratch1, vPtr->vValue );
	strcat( dScratch1, Par2 );

	SetVariable( Par1, dScratch1 );

	// release the buffer space
	free( dScratch1 );

}	// end of StrCatStatement

void IntStrStatement( char *Text )
{
	ExtractParameters( "INTSTR", "sI", Text );

	SetVariable( Par1, Par2 );

}	// end of IntStrStatement

void StrIntStatement( char *Text )
{
	ExtractParameters( "STRINT", "iS", Text );

	iScratch1=atol( Par2 );
	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of StrIntStatement

void NumStrStatement( char *Text )
{
	ExtractParameters( "NUMSTR", "sF", Text );

	SetVariable( Par1, Par2 );

}	// end of NumStrStatement

void StrNumStatement( char *Text )
{
	ExtractParameters( "STRNUM", "fS", Text );

	iFloat1=atof( Par2 );
	sprintf( Scratch1, "%f", iFloat1 );

	SetVariable( Par1, Scratch1 );

}	// end of StrNumStatement

void StrLocStatement( char *Text )
{
	ExtractParameters( "STRLOC", "iSS", Text );

	dScratch1=strstr( Par2, Par3 );

	if ( dScratch1 == NULL )
			iScratch1=0;
	else
			iScratch1 = ( int ) ( dScratch1 - Par2 );

	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of StrLocStatement

void StrCmpStatement( char *Text )
{
	ExtractParameters( "STRCMP", "iSS", Text );

	iScratch1=strcmp( Par2, Par3 );

	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of StrCmpStatement

void StrCmpiStatement( char *Text )
{
	ExtractParameters( "STRCMPI", "iSS", Text );

	iScratch1=strcasecmp( Par2, Par3 );

	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of StrCmpiStatement

void AscValStatement( char *Text )
{
	unsigned char Val;
	ExtractParameters( "ASCVAL", "iS", Text );

	Val=*Par2;

	iScratch1=( int ) Val;

	sprintf( Scratch1, "%ld", iScratch1 );

	SetVariable( Par1, Scratch1 );

}	// end of AscValStatement

void MidStrStatement( char *Text )
{
	ExtractParameters( "MIDSTR", "sSII", Text );

	iScratch1=atol( Par3 );
	iScratch2=atol( Par4 );

	if ( iScratch1 < 1 )
			ReportError( SYNTAX, BAD_MIDSTR_PAR, "Third parameter ( %s ) less than one", Par3 );

	if ( iScratch1 > strlen( Par2 ) )
			ReportError( SYNTAX, BAD_MIDSTR_PAR, "Third parameter ( %s ) higher than the length of %s", Par3, Par2 );

	if ( iScratch1 > iScratch2 )
			ReportError( SYNTAX, BAD_MIDSTR_PAR, "Third parameter ( %s ) larger than the fourth parameter ( %s )", Par3, Par4 );

	iScratch1--;
	iScratch2--;

	for ( iScratch3=iScratch1; iScratch3<=iScratch2; iScratch3++ ) {
		if ( Par2[ ( int ) iScratch3 ] == EOS )
				break;
		Scratch1[ ( int ) ( iScratch3-iScratch1 ) ]=Par2[ ( int ) iScratch3 ];
	}

	Scratch1[ ( int ) ( iScratch3-iScratch1 ) ]=EOS;

	SetVariable( Par1, Scratch1 );

}	// end of MidStrStatement

void StrrevStatement( char *Text )
{
	ExtractParameters( "STRREV", "sS", Text );

	strrev( Par2 );

	SetVariable( Par1, Par2 );

}	// end of StrrevStatement

char *strrev( char *str )
{
      char *p1, *p2;

      for ( p1 = str, p2 = str + strlen( str ) - 1; p2 > p1; ++p1, --p2 ) {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }

      return ( str );

}	// end of strrev

void RepstrStatement( char *Text )
{
	int Length;

	ExtractParameters( "REPSTR", "sSI", Text );

	iScratch1=atol( Par3 );
	iScratch1 = ( iScratch1 < 0L ) ? 1L : iScratch1;

	Length=strlen( Par2 );

	if ( Length > 0 ) {
		// allocate buffer space big enough for Par2 * Par3
		dScratch1=( char * ) malloc( ( size_t ) ( Length * ( int ) ( iScratch1+1 ) ) );

		if ( dScratch1 == NULL )
				ReportError( GENERAL, NO_MEMORY, "REPSTR: Can't allocate memory for char *dScratch1" );

		strcpy( dScratch1, Par2 );

		for ( iScratch2=1L; iScratch2 < iScratch1; iScratch2++ )
				strcat ( dScratch1, Par2 );

		SetVariable( Par1, dScratch1 );

		// release the buffer space
		free( dScratch1 );
	}
	else
			SetVariable( Par1, "" );

}	// end of RepstrStatement

void SystemStatement( char *Text )
{
	RemoveCommandWord( Text, "SYSTEM" );

	if ( strlen( Text ) == 0 ) {
		system( "command" );
	}
	else
			system( Text );

}	// end of SystemStatement

void ReplaceVariables( char *Expression, int Mode )
{
	vList *vPtr;
	static char ParsedExp[ 100 ]="", Variable[ 100 ]="", pValue[ 100 ]="";
	int i, j, k;

	STRING_VAR=FALSE;

	CheckBracketsBalance( Expression );

	BlankString( ParsedExp, EOS, 98 );
	BlankString( Variable, EOS, 98 );

	for ( i=0, k=0; Expression[ i ] != EOS; i++, k++ ) {

		// variable names must start with a letter or an underscore _ if it is a system provided variable
		if ( isalpha( Expression[ i ] ) || ( Expression[ i ] == '_') ) {

			// although they can contain numbers and/or underscores _
			for ( j=i; isalnum( Expression[ j ] ) || Expression[ j ] == '_'; j++ )
					Variable[ j-i ]=Expression[ j ];

			if ( ( Expression[ j ] == '%') || ( Expression[ j ] == '$') ) {
				Variable[ j-i ]=Expression[ j ];
				j++;
			}

			 Variable[ j-i ]=EOS;

			// check for validity of variable
			 vPtr=LookUp( Variable, ALL );

			 // not recognised - flag error
			 if ( vPtr == NULL )
					ReportError( SYNTAX, UNKNOWN_VARIABLE, "Symbol not found in the Variable Table: %s", Variable );

			if ( vPtr->Type == 's')
					STRING_VAR=TRUE;

			if ( Mode == CALCULATE )
					strcat( ParsedExp, vPtr->vValue );
			else {
				if ( STRING_VAR == FALSE ) {
					if ( Lastch( vPtr->vName ) == '%')
							sprintf( pValue, "%ld", atol( vPtr->vValue ) );
					else
							sprintf( pValue, "%.*f", WIDTH, atof( vPtr->vValue ) );

					strcat( ParsedExp, pValue );
				}
				else {
 					// string variable, just append it
					strcat( ParsedExp, vPtr->vValue );
				}
			}

			i += ( ( strlen( Variable ) )-1 );
			k = ( strlen( ParsedExp )-1 );
		}
		else
				ParsedExp[ k ] = Expression[ i ];
	}

	ParsedExp[ k ]=EOS;
	strncpy( Expression, ParsedExp, strlen( ParsedExp ) );
	Expression[ k ] = EOS;

}	// ReplaceVariables

vList *LookUp( char *vName, int Scope )
{
	vList *vPtr;

	// is it a local variable
	if ( ( Scope == ALL ) || ( Scope != GLOBAL ) ) {
		for ( vPtr=vTable[ ( Scope > PROC_CALLS ) ? Scope : PROC_CALLS ];
				vPtr != NULL; vPtr = vPtr->Next )
				if ( strcmp( vName, vPtr->vName ) == 0 )
						return ( vPtr );
	}

	if ( ( Scope == ALL ) || ( Scope == GLOBAL ) ) {
		// is it a global variable
		for ( vPtr=vTable[ GLOBAL ]; vPtr != NULL; vPtr = vPtr->Next )
				if ( strcmp( vName, vPtr->vName ) == 0 )
						return ( vPtr );
	}

	return ( NULL );

}	// end of LookUp

vList *SetVariable( char *vName, char *vValue )
{
	vList *vPtr;
	static char vTemp[ 100 ]="";
	int i, Length=strlen( vValue );
	double Answer=0.0;

	if ( ( Length != 0 ) && ( Lastch( vName ) != '$') && ( *vValue != QUOTE ) ) {
		ReplaceVariables( vValue, CALCULATE );
		if ( strpbrk( vValue, "+-/^*" ) != NULL ) {
			strcpy( Express, vValue );
			GetExpression( &Answer );

			if ( Lastch( vName ) == '%')
					sprintf( vValue, "%ld", ( long ) Answer );
			else
					sprintf( vValue, "%f", ( float ) Answer );
		}
	}

	if ( ( vPtr = LookUp( vName, ALL ) ) == NULL )
			ReportError( SYNTAX, VAR_NOT_EXIST, NULL );

	else {
		if ( vPtr->Constant == TRUE )
				ReportError( SYNTAX, CANNOT_MOD_CON, NULL );
		else
				free( ( void * ) vPtr->vValue );
	}

	// check for empty values etc
	switch ( Lastch( vName ) ) {
		// integer value
		case '%' : 	( Length == 0 ) ? strcpy( vTemp, "0" )
								 : strcpy( vTemp, vValue );
					break;

		// string value
		case '$' : 	if ( Length != 0 ) {
						// take out the quote marks?
						if ( vValue[ 0 ] == QUOTE ) {
							for ( i=1; i<Length; i++ )
									vTemp[ i-1 ]=vValue[ i ];

							vTemp[ Length-2 ]=EOS;
						}
						else
								strcpy( vTemp, vValue );
					}
					else
							strcpy( vTemp, "" );
					break;

		// floating point value
		default  :  ( Length == 0 ) ? strcpy( vTemp, "0.0" )
								 : strcpy( vTemp, vValue );
					break;
	}

	if ( ( vPtr->vValue = ( char * ) strdup( vTemp ) ) == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for the value of Variable %s", vName );

	return ( vPtr );

}	// end of SetVariable

vList *FixVariable( char *vName, char *vValue, int Offset, char *Passed )
{
	vList *vPtr;
	static char vTemp[ 100 ]="";
	int i, ReadOnly, Length=strlen( vValue );
	double Answer=0.0;

	if ( strstr( vName, " \t" ) != NULL )
			ReportError( SYNTAX, SPACES_IN_NAME, "\"%s\"", vName );

	// is this a CONSTANT definition
	ReadOnly = ( Offset == CONSTANT ) ? TRUE : FALSE;
	Offset = ( Offset < 0 ) ? 0 : Offset;

	if ( ( Length != 0 ) && ( Lastch( vName ) != '$') && ( *vValue != QUOTE ) ) {
		ReplaceVariables( vValue, CALCULATE );
		if ( strpbrk( vValue, "+-/^*" ) != NULL ) {
			strcpy( Express, vValue );
			GetExpression( &Answer );

			if ( Lastch( vName ) == '%')
					sprintf( vValue, "%ld", ( long ) Answer );
			else
					sprintf( vValue, "%f", ( float ) Answer );
		}
	}

	if ( ( vPtr = LookUp( vName, Offset ) ) == NULL ) {
		vPtr = ( vList * ) malloc( sizeof( *vPtr ) );
		if ( vPtr == NULL || ( vPtr->vName = ( char * ) strdup( vName ) ) == NULL )
					ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for Variable %s", vName );

		// set type by Lastch( vName )
		switch ( Lastch( vName ) ) {
			// integer value
			case '%' : 	vPtr->Type='i';
						( Length == 0 ) ? sprintf( vTemp, "0" ) : sprintf( vTemp, "%ld", atol( vValue ) );
						break;

			// string value
			case '$' : 	vPtr->Type='s';
						if ( Length != 0 ) {
							// take out the quote marks?
							if ( vValue[ 0 ] == QUOTE ) {
								for ( i=1; i<Length; i++ )
										vTemp[ i-1 ]=vValue[ i ];

								vTemp[ Length-2 ]=EOS;
							}
							else
									strcpy( vTemp, vValue );
						}
						else
								strcpy( vTemp, "" );
						break;

			// floating point value
			default  :  vPtr->Type='f';
						( Length == 0 ) ? sprintf( vTemp, "0.0" ) : sprintf( vTemp, "%f", ( float ) atof( vValue ) );
						break;
		}

		if ( ( vPtr->vValue = ( char * ) strdup( vTemp ) ) == NULL )
				ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for the value of Variable %s", vName );

		vPtr->Next=vTable[ Offset ];
		vPtr->Offset=Offset;
		vPtr->Constant=ReadOnly;

		if ( Passed != NULL ) {
			if ( ( vPtr->Passed = ( char * ) strdup( Passed ) ) == NULL )
					ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for the Passed name of Variable %s", vName );
		}
		else
				vPtr->Passed=NULL;

		vTable[ Offset ]=vPtr;
	}
	else
			ReportError( SYNTAX, VAR_EXISTS, "%s", vName );

	return ( vPtr );

}	// end of FixVariable

void KillVariables( void )
{
	vList *vPtr=NULL, *nPtr=NULL;

	for ( vPtr=vTable[ PROC_CALLS+1 ]; vPtr != NULL; vPtr = nPtr ) {
		nPtr=vPtr->Next;

		if ( vPtr->Passed != NULL ) {
			SetVariable( vPtr->Passed, vPtr->vValue );
			free( ( void * ) vPtr->Passed );
		}

		// free up the rest of the memory of the Killed variable
		vPtr->Next=NULL;
		vPtr->Type=0;
		free( ( void * ) vPtr->vName );
		free( ( void * ) vPtr->vValue );
		free( ( void * ) vPtr );
	}

	vTable[ PROC_CALLS+1 ]=NULL;

}	// end of KillVariables

int CheckForVariableOperations( char *Line, int Offset, int Action )
{
	static char *Ptr, Seps[]=",\n";
	static char Setting[ 50 ]="";
	int Response=FALSE;

	Ptr=strtok( Line, Seps );

	while ( Ptr != NULL ) {
		strcpy( Setting, Ptr );
		Response=ExtractSetElements( Setting, Offset, Action );
		Ptr=strtok( NULL, Seps );
	}

	return ( Response );

}	// end of CheckForVariableOperations

int ExtractSetElements( char *Setting, int Offset, int Action )
{
	static char *Ptr=NULL, vName[ 30 ]="", vValue[ 100 ]="";
	int i, Response;

	Ptr=strstr( Setting, "=" );

	if ( Ptr == NULL ) {

		RemoveWhitespace( Setting );

		if ( Action == SET )
				Response = ( SetVariable( Setting, "" ) == NULL ) ? FALSE : TRUE;
		else
				Response = ( FixVariable( Setting, "", Offset, NULL ) == NULL ) ? FALSE : TRUE;
	}
	else {
		for ( i=0; Setting[ i ] != '='; i++ )
				vName[ i ]=Setting[ i ];

		vName[ i ]=EOS;

		strcpy( vValue, ++Ptr );

		RemoveWhitespace( vName );
		RemoveWhitespace( vValue );

		if ( Action == SET )
				Response = ( SetVariable( vName, vValue ) == NULL ) ? FALSE : TRUE;
		else
				Response = ( FixVariable( vName, vValue, Offset, NULL ) == NULL ) ? FALSE : TRUE;
	}

	return ( Response );

}	// end of ExtractSetElements

int WhileStatement( char *Text )
{
	int i, Response, DeepestChanged, StartScanLevel;

	RemoveCommandWord( Text, "WHILE" );

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, NO_WHILE_CONDITION, NULL );

	if ( ( Text[ 0 ] != LEFT_PAREN ) || ( Lastch( Text ) != RIGHT_PAREN ) )
			ReportError( SYNTAX, NO_WHILE_BRACKETS, NULL );

	if ( ++WHILE_DEPTH == MAX_WHILE_DEPTH )
			ReportError( GENERAL, WHILE_TOO_DEEP, "WHILE nesting is limited to a depth of %d WHILE/ENDWHILE loops", MAX_WHILE_DEPTH );

	DeepestChanged = ( WHILE_DEPTH > DEEPEST_WHILE ) ? 0 : 1;

	DEEPEST_WHILE = ( WHILE_DEPTH > DEEPEST_WHILE )
			? WHILE_DEPTH : DEEPEST_WHILE;

	// record the line for the endwhile statement to reference
	wTable[ WHILE_DEPTH ]=( While * ) malloc( sizeof( While ) );

	if ( wTable[ WHILE_DEPTH ] == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for WhileElement[ %d ]", WHILE_DEPTH );

	wTable[ WHILE_DEPTH ]->WhileExp = strdup( Text );
	wTable[ WHILE_DEPTH ]->LoopStartLine=CURRENT_LINE;

	if ( wTable[ WHILE_DEPTH ]->WhileExp == NULL )
			ReportError( GENERAL, NO_MEMORY, "Can't allocate memory for WhileElement[ %d ]->WhileExp", WHILE_DEPTH );

	ReplaceVariables( Text, CALCULATE );
	Response=LogicResolve( Text );

	if ( Response != TRUE ) {

		if ( DeepestChanged == 0 )
				DEEPEST_WHILE--;

		free( ( void * ) wTable[ WHILE_DEPTH ]->WhileExp );
		free( ( void * ) wTable[ WHILE_DEPTH ] );
		wTable[ WHILE_DEPTH ]=NULL;

		StartScanLevel=WHILE_DEPTH;

		for ( i=CURRENT_LINE+1; i < LINES_READ; i++ ) {
			if ( WHILE_DEPTH < StartScanLevel )
					break;
			if ( strcmp( "WHILE", PullFirstWord( Source[ i ] ) ) == 0 )
					WHILE_DEPTH++;
			else if ( strcmp( "ENDWHILE", PullFirstWord( Source[ i ] ) ) == 0 )
					WHILE_DEPTH--;
			else
					continue;
		}
		return ( i-1 );
	}

	return ( CURRENT_LINE );

}	// end of WhileStatement

int EndwhileStatement( char *Text )
{
	static char WhileComp[ 100 ]="";
	int i;

	if ( WHILE_DEPTH == 0 )
			ReportError( SYNTAX, MISPLACED_ENDWHILE, NULL );

	// check we haven't got any unwanted parameters
	RemoveCommandWord( Text, "ENDWHILE" );

	// error if there are parameters
	if ( strlen( Text ) != 0 )
			ReportError( SYNTAX, ENDWHILE_WITH_PARS, NULL );

	strcpy( WhileComp, wTable[ WHILE_DEPTH ]->WhileExp );

	ReplaceVariables( WhileComp, CALCULATE );

	// check whether the loop condition is still true
	if ( LogicResolve( WhileComp ) == TRUE ) {
		// loop back to the start of the loop
		return ( wTable[ WHILE_DEPTH ]->LoopStartLine );
	}
	else {
		// exit from loop and clear up other deeper whiles
		for ( i=WHILE_DEPTH; i<= DEEPEST_WHILE; i++ ) {
			if ( wTable[ i ] != NULL ) {
				free( ( void * ) wTable[ WHILE_DEPTH ]->WhileExp );
				free( ( void * ) wTable[ WHILE_DEPTH ] );
				wTable[ WHILE_DEPTH ]=NULL;
			}
		}
		WHILE_DEPTH--;

		return ( CURRENT_LINE );
	}

}	// end of EndwhileStatement

void ForegroundStatement( char *Text )
{
	ExtractParameters( "FOREGROUND", "I", Text );

	textcolor( atoi( Par1 ) );

}	// end of ForegroundStatement

void BackgroundStatement( char *Text )
{
	ExtractParameters( "BACKGROUND", "I", Text );

	textbackground( atoi( Par1 ) );

}	// end of BackgroundStatement

void CursorStatement(char *Text)
{
	int Response;

	static char *Pars[] = {
		"OFF",
		"ON",
		NULL
	};

	Function="CURSOR";
	Syntax="must be followed by OFF or ON";

	RemoveCommandWord(Text, Function);

	if ( strlen( Text ) == 0 )
			ReportError( SYNTAX, FUNC_NO_PARS, "%s %s", Function, Syntax );

	for ( Response = 0; Response < 2; Response++ ) {
		if ( strcmp( Text, Pars[ Response ] ) == 0 )
				break;
	}

	if ( Response > 1 )
			ReportError( SYNTAX, UNRECOG_OPTION, "%s %s\n> \"%s\" is an invalid option", Function, Syntax, Text );

	switch ( Response ) {
		// off
		case  1: cursor_off();
				 break;

		// on
		case  2: cursor_on();
				 break;

		// in case things go awry
		default: break;
	 }

}	// end of CursorStatement

void HelpRequest( char *HelpString )
{
	char szHelpURL[ 512 ];
	char BinaryFolder[ 256 ] = "";
	int nPathLength;

	// get the path to the binary
	readlink( "/proc/self/exe", BinaryFolder, sizeof( BinaryFolder ) );

	nPathLength = strlen( BinaryFolder ) - 5 ;
	BinaryFolder[ nPathLength ] = 0x00;

	if ( HelpString == NULL ) {
		sprintf( szHelpURL, "xdg-open \"%ssl_help/index.html\"", BinaryFolder );
	}
	else {
		SetToLowercase( HelpString );
		sprintf( szHelpURL, "xdg-open \"%ssl_help/command_%s.html\"", BinaryFolder, HelpString );
	}

	system( szHelpURL );

}	// end of HelpRequest
