#ifndef STRUCTS_H
#define STRUCTS_H

/* used to hold the variable names and values */
struct vList {
	vList 	*Next;
	char 		*vName;
	char 		*vValue;
	int  		Offset;
	int 		Constant;
	char 		*Passed;
	char 		Type;
};

/* used to hold the list of recognised Labels */
struct Label {
	char		*Name;
	int			Line;
	Label		*Next;
};

/* used to hold the list of recognised Procs */
struct Proc {
	char		*Name;
	int			Line;
	Proc		*Next;
};

/* used to hold the list of active files */
struct File {
	char		*Name;
	FILE 		*Handle;
	int			Open;
	File		*Prev;
	File		*Next;
};

/* For loop structure */
struct For {
	char 		*vName;
	float 	Cease;
	float 	Step;
	int 		LoopStartLine;
};

/* While loop structure */
struct While {
	char 	*WhileExp;
	int 	LoopStartLine;
};

/* Do loop structure */
struct Do {
	char 	*UntilExp;
	int 	LoopBackLine;
};

#endif
